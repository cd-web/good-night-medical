﻿using GenericBackend.PaymentProcessor.Models;
using GenericBackend.PaymentProcessor.Models.Payment;

namespace GenericBackend.PaymentProcessor.Core.Interfaces
{
    public interface IPaymentProcessor
    {
        void SetCreditCard(CreditCardModel creditCardModelModel);
        void SetShippingBillingAddress(ShipmentModel shipmentModel, ShipmentModel billingModel, string email);
        TransactionModel InitializeChargeRequestAndExecute(string itemOrTypeName, string shipping, string itemPrice, string total);
    }
}