﻿//using System.Collections.Generic;
//using System.Linq;
//using System.Runtime.InteropServices;
//using System.Threading.Tasks;
//using System.Web.Http;
//using System.Web.Http.Results;
//using GenericBackend.Areas.Admin.Controllers;
//using GenericBackend.DataModels.GoodNightMedical;
//using GenericBackend.Repository;
//using GenericBackend.Tests.ControllersTests;
//using NUnit.Framework;

//namespace GenericBackend.Tests.AdminTests.ControllersTests
//{
//    [TestFixture]
//    public class AnalyticsControlerTests
//    {
//        #region Test Configuration

//        //Test data Samples

//        static readonly AnalyticsInfo[] AnalyticsInfoArray = {
//            new AnalyticsInfo {
//        RequestPageUrl= "https://www.google.com.ua/search?q=sleep+apnea+machine+rent&ie=utf-8&oe=utf-8&client=firefox-b&gfe_rd=cr&ei=rPXHV5WNOaLP8geWkIXQBg",
//        LandingPageUrl= "http://programs.goodnightmedical.com/#/rent/catalog",
//        QueryParameters=new Dictionary<string,string> {
//            {"q","sleep+apnea+machine+rent"},
//            {"ie", "utf-8"},
//            {"client", "firefox-b" },
//            {"gfe_rd", "cr"},
//            {"ei", "rPXHV5WNOaLP8geWkIXQBg"}
//        }
//    },
//    new AnalyticsInfo {
//        RequestPageUrl= "https://yandex.ua/search/?lr=147&msid=1472722851.25328.22901.29654&text=sleep%20apnea%20machine%20rent&p=0",
//        LandingPageUrl= "http://programs.goodnightmedical.com/#/rent/catalog",
//        QueryParameters= new Dictionary<string,string> {
//            { "lr", "147" },
//            {"msid", "1472722851.25328.22901.29654"},
//            {"text","sleep%20apnea%20machine%20rent"},
//            { "p", "0"}
//        }
//       }
//    };

//        //Dependancy Mocks
//        private readonly UnitOfWorkMock _unitOfWorkMock = new UnitOfWorkMock();
//        private readonly AnalyticsInfoRepositoryMock _analyticsInfoRepositoryMock = new AnalyticsInfoRepositoryMock();
//        class AnalyticsInfoRepositoryMock : MongoRepository<AnalyticsInfo>, IMongoRepository<AnalyticsInfo>
//        {
//            public new IMongoRepository<AnalyticsInfo> GetList()
//            {
//                _result = AnalyticsInfoArray;
//                return null;

//            }
//        }

//        //Output stubs
//        private static AnalyticsInfo[] _result = new AnalyticsInfo[0];

//        //Unit Under Test
//        private AnalyticsController _controller;

//        [SetUp]
//        public void TestSetup()
//        {
//            _unitOfWorkMock.AnalyticsInfo = _analyticsInfoRepositoryMock;
//            _controller = new AnalyticsController(_unitOfWorkMock);

//        }
//        #endregion Test Configuration


//        #region Task<IHttpActionResult> Get() Tests

//        [Test]
//        public void Get_ReturnsResultFormMongoRepositoryGetListCall()
//        {
//            OkNegotiatedContentResult<AnalyticsInfo[]> rez =  _controller.Get().Result as OkNegotiatedContentResult<AnalyticsInfo[]>;
//            if (rez != null)
//            {
//                for (int i=0; i< rez.Content.Length;i++)
//                {
//                    Assert.AreEqual(AnalyticsInfoArray[i].RequestPageUrl, rez.Content[i].RequestPageUrl);
//                    Assert.AreEqual(AnalyticsInfoArray[i].LandingPageUrl, rez.Content[i].LandingPageUrl);
//                    if (rez.Content[i].QueryParameters != null)
//                    {
//                        for (int j = 0; j < rez.Content[i].QueryParameters.Count; j++)
//                        {
//                            Assert.AreEqual(AnalyticsInfoArray[i].QueryParameters[AnalyticsInfoArray[i].QueryParameters.Keys.ElementAt(j)],
//                                rez.Content[i].QueryParameters[AnalyticsInfoArray[i].QueryParameters.Keys.ElementAt(j)]);
//                        }
//                    }
                    
//                }
                
//            }
//        }
//        #endregion Task<IHttpActionResult> Get() Tests
       
//    }
//}

