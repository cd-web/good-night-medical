﻿using GenericBackend.DataModels.GoodNightMedical;
using GenericBackend.DataModels.GoodNightMedical.PatientEntities;
using GenericBackend.Repository;
using GenericBackend.UnitOfWork.GoodNightMedical;

namespace GenericBackend.Tests
{
    internal class UnitOfWorkMock : IUnitOfWork
    {
        public IMongoRepository<Machine> Machines { get; set; }
        public IMongoRepository<RentOption> RentOptions { get; set; }
        public IMongoRepository<MachineOrder> MachineOrders { get; set; }
        public IMongoRepository<Customer> Customers { get; set; }
        public IMongoRepository<FullRentCustomer> FullRentCustomers { get; set; }
        public IMongoRepository<RentProgram> RentPrograms { get; set; }
        public IMongoRepository<Setting> Settings { get; set; }
        public IMongoRepository<Transaction> Payment { get; set; }
        public IMongoRepository<ReSupplyCustomer> ReSupplyCustomer { get; set; }
        public IMongoRepository<Patient> Patient { get; set; }
        public IMongoRepository<AnalyticsInfoItem> AnalyticsInfo { get; set; }
    }

}
