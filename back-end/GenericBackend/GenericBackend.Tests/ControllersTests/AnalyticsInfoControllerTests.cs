﻿//using System.Collections.Generic;
//using GenericBackend.Controllers;
//using GenericBackend.DataModels.GoodNightMedical;
//using GenericBackend.Repository;
//using NUnit.Framework;

//namespace GenericBackend.Tests.ControllersTests
//{
//    [TestFixture]
//    public class AnalyticsInfoControllerTests
//    {
//        #region Test Configuration
//        //Test data Samples
//        private readonly string _urlWithParams = "http//www.anydomain.any?a=123&b=456";
//        private readonly string _urlWithoutParams = "http//www.anydomain.any";

//        //Dependancy Mocks
//        private readonly UnitOfWorkMock _unitOfWorkMock = new UnitOfWorkMock();
//        private readonly AnalyticsInfoRepositoryMock _analyticsInfoRepositoryMock = new AnalyticsInfoRepositoryMock();
//        class AnalyticsInfoRepositoryMock : MongoRepository<AnalyticsInfo>, IMongoRepository<AnalyticsInfo>
//        {
//            public override AnalyticsInfo Add(AnalyticsInfo entry)
//            {
//                _result.QueryParameters = entry.QueryParameters;
//                return null;
//            }
//        }

//        //Output stubs
//        private static AnalyticsInfo _result = new AnalyticsInfo();

//        //Unit Under Test
//        private AnalyticsInfoController _controller;

//        [SetUp]
//        public void TestSetup()
//        {
//            _unitOfWorkMock.AnalyticsInfo = _analyticsInfoRepositoryMock;
//            _controller = new AnalyticsInfoController(_unitOfWorkMock);
//        }
//        #endregion Test Configuration


//        #region IHttpActionResult PostAnalyticsInfo(AnalyticsInfo analyticsInfo) Tests

//        [Test]
//        public void PostAnalyticsInfo_WhenRequestPageUrlHasNoParams_DoesntThrowException()
//        {
//            Models.AnalyticsInfo info = new Models.AnalyticsInfo
//            {
//                RequestPageUrl = _urlWithoutParams,
//                LandingPageUrl = _urlWithoutParams
//            };
//            Assert.DoesNotThrow(()=>_controller.PostAnalyticsInfo(info));
//        }

//        [Test]
//        public void PostAnalyticsInfo_WhenRequestPageUrlHasParams_ParsesItCorrectly()
//        {
//            Models.AnalyticsInfo info = new Models.AnalyticsInfo
//            {
//                RequestPageUrl = _urlWithParams,
//                LandingPageUrl = _urlWithoutParams
//            };
//            Dictionary<string, string> urlParams = new Dictionary<string, string>
//            {
//                {"a", "123"},
//                {"b", "456"}
//            };
//            _controller.PostAnalyticsInfo(info);
//            string expValueA, expValueB;
//            string actValueA, actValueB;
//            urlParams.TryGetValue("a", out expValueA);
//            _result.QueryParameters.TryGetValue("a", out actValueA);
//            urlParams.TryGetValue("b", out expValueB);
//            _result.QueryParameters.TryGetValue("b", out actValueB);
//            Assert.AreEqual(expValueA, actValueA);
//            Assert.AreEqual(expValueB, actValueB);
//        }
//        #endregion IHttpActionResult PostAnalyticsInfo(AnalyticsInfo analyticsInfo) Tests
        
//    }
//}
