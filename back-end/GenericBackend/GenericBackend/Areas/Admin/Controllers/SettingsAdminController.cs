﻿using System.Linq;
using System.Web.Http;
using AutoMapper;
using GenericBackend.Areas.Admin.Models.Settings;
using GenericBackend.DataModels.GoodNightMedical;
using GenericBackend.Repository;
using GenericBackend.UnitOfWork.GoodNightMedical;

namespace GenericBackend.Areas.Admin.Controllers
{
    [RoutePrefix("admin/settings")]
    [AllowAnonymous]
    public class SettingsAdminController : ApiController
    {
        private readonly IMongoRepository<Setting> _repository;

        public SettingsAdminController(IUnitOfWork unitOfWork)
        {
            _repository = unitOfWork.Settings;
        }

        [HttpGet]
        [Route("")]
        public IHttpActionResult Get()
        {
            var settings = _repository.FirstOrDefault(x => x.ParamKey == "IsPayment");

            return Ok(Mapper.Map<SettingAdminModel>(settings));
        }

        [HttpGet]
        [Route("{id}")]
        public IHttpActionResult Get(string id)
        {
            var setting = _repository.GetById(id);

            if (setting == null)
            {
                return NotFound();
            }

            return Ok(Mapper.Map<SettingAdminModel>(setting));
        }

        [HttpPost]
        [Route("")]
        public IHttpActionResult Post([FromBody]SettingAdminModel adminModel)
        {
            var setting = Mapper.Map<Setting>(adminModel);

            _repository.Add(setting);

            return Ok();
        }

        [HttpPost]
        [Route("update")]
        public IHttpActionResult Put([FromBody]SettingAdminModel adminModel)
        {
            var setting = _repository.FirstOrDefault(x => x.Id == adminModel.Id);

            if (setting != null)
            {
                setting.ParamValue = adminModel.ParamValue;
                _repository.Update(setting);
            }

            return Ok();
        }
    }
}
