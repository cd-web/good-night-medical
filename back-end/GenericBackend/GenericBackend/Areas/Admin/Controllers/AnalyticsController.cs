﻿using System.Threading.Tasks;
using System.Web.Http;
using GenericBackend.DataModels.GoodNightMedical;
using GenericBackend.Repository;
using GenericBackend.UnitOfWork.GoodNightMedical;

namespace GenericBackend.Areas.Admin.Controllers
{
    [RoutePrefix("admin/analytics")]
    public class AnalyticsController : ApiController
    {
        private readonly IMongoRepository<AnalyticsInfoItem> _analyticsInfoRepository;

        public AnalyticsController(IUnitOfWork unitOfWork)
        {
            _analyticsInfoRepository = unitOfWork.AnalyticsInfo;
        }

        [HttpGet]
        [Route("")]
        public async Task<IHttpActionResult> Get()
        {
            var analyticsInfo = await _analyticsInfoRepository.GetList();

            return Ok(analyticsInfo);
        }
    }
}
