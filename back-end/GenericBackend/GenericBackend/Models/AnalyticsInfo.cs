﻿using System;
using System.Collections.Generic;

namespace GenericBackend.Models
{
    public class AnalyticsInfo
    {
        public string LandingPageUrl { get; set; }
        public string Request { get; set; }
        public string RequestText { get; set; }
        public List<Analytics> Analytics { get; set; }
    }

    public class Analytics
    {
        public string RequestPageUrl { get; set; }
        public DateTime RequestTime { get; set; }
        public Dictionary<string, string> QueryParameters { get; set; }
    }


    public class IncomeLandingInfo
    {
        public string LandingPageUrl { get; set; }
        public string RequestPageUrl { get; set; }
        public DateTime RequestTime { get; set; }
    }
}
