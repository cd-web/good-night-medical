﻿using AutoMapper;
using GenericBackend.Core.Enums;
using GenericBackend.DataModels.GoodNightMedical;
using GenericBackend.Helpers.Mappings.Interfaces;

namespace GenericBackend.Models.Machines
{
    public class CatalogMachineModel : ICustomMapping
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string ImageUrl { get; set; }
        public decimal PricePerMonth { get; set; }
        public bool Refurbished { get; set; }
        public ProgramType Type { get; set; }
        public Company Company { get; set; }
        public string Ships { get; set; }
        public bool IsPayment { get; set; }

        public void CreateMappings(IConfiguration configuration)
        {
            configuration.CreateMap<Machine, CatalogMachineModel>();

        }
    }
}