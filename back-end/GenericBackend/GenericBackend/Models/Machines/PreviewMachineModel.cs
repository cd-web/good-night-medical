﻿using AutoMapper;
using GenericBackend.DataModels.GoodNightMedical;
using GenericBackend.Helpers.Mappings.Interfaces;

namespace GenericBackend.Models.Machines
{
    public class PreviewMachineModel : ICustomMapping
    {
        public string Id { get; set; }
        public string Title { get; set; }
        public string Model { get; set; }
        public string Description { get; set; }
        public string Price { get; set; }
        public string ImageUrl { get; set; }
        public string Ships { get; set; }
        public bool IsPayment { get; set; }

        public void CreateMappings(IConfiguration configuration)
        {
            configuration.CreateMap<Machine, PreviewMachineModel>()
                .ForMember(m => m.Title, opt => opt.MapFrom(x => x.Type.ToString()))
                .ForMember(m => m.Model, opt => opt.MapFrom(x => x.Name))
                .ForMember(m => m.IsPayment, opt => opt.Ignore())
                .ForMember(m => m.Price, opt => opt.MapFrom(x => x.PricePerMonth.ToString()));
        }
    }
}