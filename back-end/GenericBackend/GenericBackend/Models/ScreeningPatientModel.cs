﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using AutoMapper;
using GenericBackend.DataModels.GoodNightMedical.PatientEntities;
using GenericBackend.Helpers.Mappings.Interfaces;

namespace GenericBackend.Models
{
   public class ScreeningPatientModel : ICustomMapping
    {
        public string FullName { get; set; }
        public Gender Gender { get; set; }
        public string PhoneNumber { get; set; }

        [Required]
        public string Email { get; set; }
        public List<Address> Address { get; set; }
        public DateTime DateOfBirth { get; set; }
        public List<ScreeningDataItem> ScreeningData { get; set; }

       public void CreateMappings(IConfiguration configuration)
       {
           configuration.CreateMap<ScreeningDataItem, DataModels.GoodNightMedical.ScreeningDataItem>();

           configuration.CreateMap<ScreeningPatientModel, Patient>()
               .ForMember(x => x.Gender, opt => opt.MapFrom(x => x.Gender))
               .ForMember(x => x.ScreeningData, opt => opt.MapFrom(x => x.ScreeningData));
       }

    }

    public class Address
    {
        public string State { get; set; }
        public string City { get; set; }
        public string ZipCode { get; set; }
        public string PlainAddress { get; set; }
        public bool Preferrable { get; set; }
    }
}
