﻿
using System;
using AutoMapper;
using GenericBackend.DataModels.GoodNightMedical;
using GenericBackend.DataModels.GoodNightMedical.PatientEntities;
using GenericBackend.Helpers.Mappings.Interfaces;

namespace GenericBackend.Models
{
    public class ReSupplyCustomerModel : ICustomMapping
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public DateTime BirthDate { get; set; }
        public bool CurrentPatient { get; set; }
        public string OfferCode { get; set; }

        public void CreateMappings(IConfiguration configuration)
        {
            configuration.CreateMap<ReSupplyCustomerModel, Patient>()
                .ForMember(x => x.FullName, opt => opt.MapFrom(x => $"{x.FirstName} {x.LastName}"))
                .ForMember(x => x.DateOfBirth, opt => opt.MapFrom(x => x.BirthDate))
                .ForMember(x => x.Gender, opt => opt.UseValue(Gender.Male));

            configuration.CreateMap<ReSupplyCustomerModel, ReSupplyCustomer>();
        }
    }
}
