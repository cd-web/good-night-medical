﻿namespace GenericBackend.Models
{
    public class ScreeningDataItem
    {
        public string Question { get; set; }
        public string Answer { get; set; }
    }
}
