﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;
using GenericBackend.Core.Enums;
using GenericBackend.DataModels.GoodNightMedical.PatientEntities;
using GenericBackend.DataModels.GoodNightMedical.Requests;
using GenericBackend.Helpers.Mappings.Interfaces;
using GenericBackend.UnitOfWork.GoodNightMedical.Models.Patient;

namespace GenericBackend.Models.Catalog
{
    public class HelpPickUpModel : ICustomMapping
    {
        public string FullName { get; set; }
        public string Email { get; set; }
        public bool CurrentPatient { get; set; }
        public string Address { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string Message { get; set; }
        public string PhysicianInformation { get; set; }
        public List<DocumentModel> Documents { get; set; }
        public TreatmentModel Treatment { get; set; }

        public void CreateMappings(IConfiguration configuration)
        {
            configuration.CreateMap<HelpPickUpModel, PickCpapRequest>();

            configuration.CreateMap<DocumentModel, Document>();

            configuration.CreateMap<TreatmentModel, Treatment>();

            configuration.CreateMap<HelpPickUpModel, Patient>()
                .ForMember(x => x.Documents, opt => opt.Ignore());
        }
    }

    public class TreatmentModel
    {
        public string Manafacturer { get; set; }
        public ProgramType Type { get; set; }
        public string MachineName { get; set; }
    }

    public class DocumentModel
    {
        public string Name { get; set; }
        public string Url { get; set; }
    }
}