﻿using AutoMapper;
using GenericBackend.Core.Enums;
using GenericBackend.DataModels.GoodNightMedical;
using GenericBackend.Helpers.Mappings.Interfaces;

namespace GenericBackend.Models.Customer
{
    public class CustomerRentInsert : ModelBase, ICustomMapping
    {
        public string FullName { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public DoctorPrescription Prescription { get; set; }
        public string Comments { get; set; }
        public string Program { get; set; }
        public string MachineId { get; set; }
        public string Origin { get; set; }

        public void CreateMappings(IConfiguration configuration)
        {
            configuration.CreateMap<CustomerRentInsert, FullRentCustomer>()
                .ForMember(m => m.DoctorPrescription, opt => opt.MapFrom(x => x.Prescription));
        }
    }
}