﻿namespace GenericBackend.Models.HomeSleepTest
{
    public class SleepRequestModel
    {
        public string FullName { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
    }
}