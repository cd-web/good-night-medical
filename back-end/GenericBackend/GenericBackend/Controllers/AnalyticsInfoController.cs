﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.Http;
using GenericBackend.Models;
using GenericBackend.MySqlAccess;
using GenericBackend.Repository;
using GenericBackend.UnitOfWork.GoodNightMedical;
using MongoDB.Driver;
using Analytics = GenericBackend.DataModels.GoodNightMedical.Analytics;

namespace GenericBackend.Controllers
{
    [AllowAnonymous]
    [RoutePrefix("api/analytics")]
    public class AnalyticsInfoController : ApiController
    {
        private readonly IMongoRepository<DataModels.GoodNightMedical.AnalyticsInfoItem> _analyticsInfoRepository;
        private readonly MySqlAccessor _mySqlAccessor = new MySqlAccessor();
        public AnalyticsInfoController(IUnitOfWork unitOfWork)
        {
            _analyticsInfoRepository = unitOfWork.AnalyticsInfo;
        }

        [HttpPost]
        [Route("")]
        public IHttpActionResult PostAnalyticsInfo(IncomeLandingInfo incomeLandingInfo)
        {
            string request, requestText;
            Dictionary<string, string> queryParameters = ExtractParams(incomeLandingInfo.RequestPageUrl, out request, out requestText);

            DataModels.GoodNightMedical.AnalyticsInfoItem infoItem = null;
            try
            {
                infoItem = _analyticsInfoRepository.First(a => a.LandingPageUrl == incomeLandingInfo.LandingPageUrl
                                                    && string.Equals(a.RequestText, requestText,
                                                        StringComparison.CurrentCultureIgnoreCase));
            }
            catch
            {
                // ignored
            }

            DataModels.GoodNightMedical.AnalyticsInfoItem newItem = new DataModels.GoodNightMedical.AnalyticsInfoItem
            {
                LandingPageUrl = incomeLandingInfo.LandingPageUrl,
                RequestText = requestText,
                Request = request,
                Analytics = new List<Analytics>
                {
                    new Analytics
                    {
                        RequestPageUrl = incomeLandingInfo.RequestPageUrl,
                        RequestTime = incomeLandingInfo.RequestTime,
                        QueryParameters = queryParameters
                    }
                }
            };

            if (infoItem!=null)
            {
                List<Analytics> currAnalytics = infoItem.Analytics;
                currAnalytics.Add(new Analytics
                {
                    RequestPageUrl = incomeLandingInfo.RequestPageUrl,
                    RequestTime = incomeLandingInfo.RequestTime,
                    QueryParameters = queryParameters
                });
                _analyticsInfoRepository.Collection.UpdateOneAsync(
                    a => a.LandingPageUrl == incomeLandingInfo.LandingPageUrl
                         && string.Equals(a.RequestText, requestText,
                             StringComparison.CurrentCultureIgnoreCase),
                    Builders<DataModels.GoodNightMedical.AnalyticsInfoItem>.Update.Set("Analytics", currAnalytics));
            }

            else
            {
                _analyticsInfoRepository.Add(newItem);
            }

            //////////////////////////////////////////////////// MySql
            _mySqlAccessor.Add(newItem);


            return Ok();
        }


        private Dictionary<string, string> ExtractParams(string url, out string request, out string requestText)
        {
            request = null;
            requestText = null;
            Dictionary<string, string> queryParams = null;
            int index = url.IndexOf('?');
            if (index > 0 && index < url.Length - 1)
            {
                string paramString = url.Substring(index + 1);
                string[] paramStrs = paramString.Split('&');
                if (paramStrs.Length > 0)
                {
                    queryParams = new Dictionary<string, string>();
                    foreach (string s in paramStrs)
                    {
                        string[] pair = s.Split('=');
                        if (pair.Length == 2)
                        {
                            queryParams.Add(pair[0], pair[1]);
                            if (pair[0] == "q" || pair[0] == "text")
                            {
                                request =
                                    string.Format(CultureInfo.CurrentUICulture, "{0}={1}", pair[0], pair[1]);
                                requestText = pair[1].ToLower().Replace('+',' '); 
                            }
                        }
                        else
                        {
                            queryParams.Add(s, s);
                        }
                    }
                }
            }
            if (request == null) request = "<direct link>";
            if (requestText == null) requestText = "<direct link>";

            return queryParams;
        }
    }
}
