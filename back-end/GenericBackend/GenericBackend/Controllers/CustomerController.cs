﻿using System.Configuration;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using AutoMapper;
using GenericBackend.DataModels.GoodNightMedical;
using GenericBackend.Models.Customer;
using GenericBackend.Notifications;
using GenericBackend.Notifications.Template;
using GenericBackend.Repository;
using GenericBackend.UnitOfWork.GoodNightMedical;

namespace GenericBackend.Controllers
{
    [AllowAnonymous]
    [RoutePrefix("api/customer")]
    public class CustomerController : ApiController
    {
        private readonly IMongoRepository<Customer> _customersRepository;
        private readonly IMongoRepository<FullRentCustomer> _fullCustomersRepository;
        private readonly IMongoRepository<Machine> _machineRepository;

        public CustomerController(IUnitOfWork unitOfWork)
        {
            _customersRepository = unitOfWork.Customers;
            _fullCustomersRepository = unitOfWork.FullRentCustomers;
            _machineRepository = unitOfWork.Machines;
        }

        [HttpGet]
        [Route("")]
        public async Task<IHttpActionResult> Get()
        {
            var customers = await _customersRepository.GetList();

            return Ok(customers);            
        }

        [HttpGet]
        [Route("{id}")]
        public IHttpActionResult Get(string id)
        {
            var customer = _customersRepository.GetById(id);

            return Ok(customer);
        }

        [HttpGet]
        [Route("seen/{id}")]
        public IHttpActionResult SetSeen(string id)
        {
            var customer = _customersRepository.GetById(id);

            if (customer == null) return Ok();

            customer.New = false;
            _customersRepository.Update(customer);

            return Ok();
        }

        /// <summary>
        /// recieve contactUs form from block1.html and footer.html (website)
        /// on admin panel it's contactusController.js (admin) for proceeding incoming customer requests
        /// </summary>
        /// <param name="model">form model of new customer</param>
        /// <returns></returns>
        [HttpPost]
        [Route("contact-us")]
        public IHttpActionResult Post([FromBody] CustomerInsert model)
        {
            var customer = Mapper.Map<Customer>(model);

            _customersRepository.Add(customer);

            var emailModel = new EmailModel
            {
                Subject = EmailTemplates.ContactUsSubject,
                Body = string.Format(EmailTemplates.ContactUsBody, customer.Email, customer.FullName, customer.Phone),
                To = ConfigurationManager.AppSettings["RecipientEmail"]
            };

            var customerModel = new EmailModel
            {
                Subject = EmailTemplates.ContactUsCustomerSubject,
                Body = string.Format(EmailTemplates.ContactUsCustomerBody, customer.Email, customer.FullName, customer.Phone),
                To = customer.Email
            };

            var emailSender = new EmailSender();

            emailSender.Initialize();

            emailSender.SendMessage(emailModel);
            emailSender.SendMessage(customerModel);

            return Ok();
        }

        /// <summary>
        /// recieve modalForm from block5.html and send this requests to the customer.
        ///  The modal is part of the modal.js.
        /// on admin panel it's rentController.js(admin) for proceeding incoming rent requests
        /// </summary>
        /// <param name="customerRent">form model of rent request</param>
        /// <returns></returns>
        [HttpPost]
        [Route("rent")]
        public IHttpActionResult PostContact([FromBody] CustomerRentInsert customerRent)
        {
            var customer = Mapper.Map<FullRentCustomer>(customerRent);
            Machine machine = null;

            if (customerRent.MachineId != null)
            {
                machine = _machineRepository.GetById(customer.MachineId);
            }

            _fullCustomersRepository.Add(customer);

            var model = new EmailModel
            {
                Subject = string.Format(EmailTemplates.RentProgramSubject, customerRent.FullName, machine == null ? customerRent.Program : machine.Name),
                Body =
                    string.Format(EmailTemplates.RentProgramBody, customerRent.Program, customerRent.Email,
                        customerRent.FullName, customerRent.Phone, customerRent.Prescription,
                        customerRent.Comments),
                To = ConfigurationManager.AppSettings["RecipientEmail"]
            };

            var customerModel = new EmailModel
            {
                Subject = string.Format(EmailTemplates.RentProgramCustomerSubject, customer.Origin),
                Body =
                    string.Format(EmailTemplates.RentProgramCustomerBody, customer.Email, customer.FullName,
                        customer.Phone, customer.DoctorPrescription, customer.Comments),
                To = customer.Email
            };

            var emailSender = new EmailSender();

            emailSender.Initialize();

            emailSender.SendMessage(model);
            emailSender.SendMessage(customerModel);

            return Ok();
        }
    }
}
