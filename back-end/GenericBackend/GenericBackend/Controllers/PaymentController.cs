﻿using System;
using System.Web.Http;
using GenericBackend.DataModels.GoodNightMedical;
using GenericBackend.Models.Payment;
using GenericBackend.PaymentProcessor.Core.Interfaces;
using GenericBackend.Repository;
using GenericBackend.UnitOfWork.GoodNightMedical;

namespace GenericBackend.Controllers
{
    [RoutePrefix("api/payment")]
    public class PaymentController : ApiController
    {
        private readonly IPaymentProcessor _paymentProcessor;
        private readonly IMongoRepository<Transaction> _paymentRepository;

        public PaymentController(
            IPaymentProcessor paymentProcessor, 
            IUnitOfWork unitOfWork)
        {
            _paymentProcessor = paymentProcessor;
            _paymentRepository = unitOfWork.Payment;
        }

        [HttpPost]
        [Route("")]
        public IHttpActionResult Post([FromBody]PaymentModel model)
        {
            _paymentProcessor.SetCreditCard(model.CreditCard);
            _paymentProcessor.SetShippingBillingAddress(model.Shipment, model.IsSameBillShip ? model.Shipment : model.Billing, model.Email);

            var result = _paymentProcessor.InitializeChargeRequestAndExecute(model.Type, model.Shipping, model.ItemPrice, model.Total);

            if (result == null)
            {
                return Ok();
            }

            _paymentRepository.Add(new Transaction
            {
                TransactionId = result.TransactionId,
                AuthCode = result.AuthCode,
                Error = result.Error,
                StatusCode = result.StatusCode
            });

            return Ok();
        }
    }
}
