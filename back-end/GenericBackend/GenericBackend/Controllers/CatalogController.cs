﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using AutoMapper;
using GenericBackend.DataModels.GoodNightMedical;
using GenericBackend.DataModels.GoodNightMedical.PatientEntities;
using GenericBackend.DataModels.GoodNightMedical.Requests;
using GenericBackend.Models.Catalog;
using GenericBackend.Models.Machines;
using GenericBackend.Repository;
using GenericBackend.UnitOfWork.GoodNightMedical;
using GenericBackend.UnitOfWork.GoodNightMedical.Interfaces;

namespace GenericBackend.Controllers
{
    [RoutePrefix("api/catalog")]
    [AllowAnonymous]
    public class CatalogController : ApiController
    {
        private readonly IMongoRepository<Machine> _machinesRepository;
        private readonly IMongoRepository<RentProgram> _rentProgramsRepository;
        private readonly IPatientWork _patientWork;
        private readonly bool _isPayment;

        public CatalogController(
            IUnitOfWork unitOfWork, 
            IPatientWork patientWork)
        {
            _patientWork = patientWork;
            _machinesRepository = unitOfWork.Machines;
            _rentProgramsRepository = unitOfWork.RentPrograms;

            var settingsRepository = unitOfWork.Settings;

            var paymentParam = settingsRepository.FirstOrDefault(x => x.ParamKey == "IsPayment");

            _isPayment = paymentParam != null && Convert.ToBoolean(paymentParam.ParamValue);

        }

        [HttpGet]
        [Route("")]
        public async Task<IHttpActionResult> Get()
        {
            var list = await _machinesRepository.GetList();
            var model = Mapper.Map<List<CatalogMachineModel>>(list);

            foreach (var catalogMachineModel in model)
            {
                var firstOrDefault = _rentProgramsRepository
                    .FirstOrDefault(x => x.Type == catalogMachineModel.Type);

                if (firstOrDefault != null)
                {
                    catalogMachineModel.Ships = firstOrDefault.CatalogBriefInfo.Ships;
                }
                catalogMachineModel.IsPayment = _isPayment;
            }

            return Ok(model);
        }

        [HttpPost]
        [Route("help-pick")]
        public IHttpActionResult Post([FromBody]HelpPickUpModel model)
        {
            var helpRequest = Mapper.Map<PickCpapRequest>(model);

            if (model.CurrentPatient)
            {
                var patient = _patientWork.Patients.FirstOrDefault(x => x.Email == model.Email);

                if (patient != null)
                {
                    helpRequest.Patient = patient;

                    _patientWork.PickCpapRequests.Add(helpRequest);

                    return Ok();
                }

                return NotFound();
            }

            var newPatient = ConvertToPatient(model);
            helpRequest.Patient = newPatient;

            _patientWork.PickCpapRequests.Add(helpRequest);

            return Ok();
        }

        #region private methods 

        private Patient ConvertToPatient(HelpPickUpModel model)
        {
            var patient = Mapper.Map<Patient>(model);
            var diagnose = new Diagnose {Treatment = Mapper.Map<Treatment>(model.Treatment)};
            var documents = Mapper.Map<List<Document>>(model.Documents);

            patient.Documents.AddRange(documents);
            patient.Diagnoses.Add(diagnose);

            _patientWork.Patients.Add(patient);

            return patient;
        }

        #endregion
    }
}
