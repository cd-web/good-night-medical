﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using AutoMapper;
using GenericBackend.DataModels.GoodNightMedical;
using GenericBackend.DataModels.GoodNightMedical.PatientEntities;
using GenericBackend.Models;
using GenericBackend.Models.HomeSleepTest;
using GenericBackend.Notifications;
using GenericBackend.UnitOfWork.GoodNightMedical.Interfaces;
using GenericBackend.Repository;
using GenericBackend.UnitOfWork.GoodNightMedical;

namespace GenericBackend.Controllers
{
    [AllowAnonymous]
    [RoutePrefix("api/patient")]
    public class PatientController : ApiController
    {
        private readonly IPatientWork _patientWork;
        private readonly IMongoRepository<ReSupplyCustomer> _reSupplyCustomerRepository;
        private readonly IMongoRepository<Patient> _patientRepository;

        public PatientController(IPatientWork patientWork, IUnitOfWork unitOfWork)
        {
            _patientWork = patientWork;
            _patientRepository = unitOfWork.Patient;
            _reSupplyCustomerRepository = unitOfWork.ReSupplyCustomer;
        }

        [HttpGet]
        [Route("list")]
        public async Task<IHttpActionResult> Get()
        {
            var patients = await _patientWork.Patients.GetList();

            return Ok(patients);
        }

        [HttpPost]
        [Route("screening")]
        public IHttpActionResult PostScreening([FromBody]ScreeningPatientModel model)
        {
            var currentPatient = _patientRepository
                .FirstOrDefault(x => x.Email == model.Email);

            if (currentPatient == null)
            {
                var patient = Mapper.Map<Patient>(model);
                _patientRepository.Add(patient);

                return Ok();
            }

            currentPatient.ScreeningData = Mapper.Map<List<DataModels.GoodNightMedical.ScreeningDataItem>>(model.ScreeningData);
            _patientRepository.Update(currentPatient);

            return Ok();
        }

        [HttpPost]
        [Route("sleep-request")]
        public IHttpActionResult PostSleep([FromBody]SleepRequestModel model)
        {
            var patient = _patientRepository.FirstOrDefault(x => x.Email == model.Email);

            if (patient != null)
            {
                //some logic if user already exist

                return Ok();
            }

            var sender = new EmailSender();

            sender.Initialize();
            //sender.SendMessage();

            return Ok();
        }

        [HttpPost]
        [Route("resupply")]
        public IHttpActionResult Post([FromBody]ReSupplyCustomerModel model)
        {
            var patient = _patientRepository.FirstOrDefault(x => x.Email == model.Email);

            if (model.CurrentPatient && patient != null)
            {
                AddResupply(patient, model);

                return Ok();
            }

            if (patient == null)
            {
                patient = Mapper.Map<Patient>(model);

                _patientRepository.Add(patient);
            }

           AddResupply(patient, model);

           return Ok();
        }

        #region private methods

        private void AddResupply(Patient patient, ReSupplyCustomerModel model)
        {
            var resupplyForExistedPatient = Mapper.Map<ReSupplyCustomer>(model);
            resupplyForExistedPatient.Customer = patient;

            _reSupplyCustomerRepository.Add(resupplyForExistedPatient);
        }

        #endregion
    }
}
