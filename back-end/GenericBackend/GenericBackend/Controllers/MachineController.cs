﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using AutoMapper;
using GenericBackend.Core.Enums;
using GenericBackend.DataModels.GoodNightMedical;
using GenericBackend.Logging;
using GenericBackend.Logging.Interfaces;
using GenericBackend.Models;
using GenericBackend.Models.Machines;
using GenericBackend.Repository;
using GenericBackend.UnitOfWork.GoodNightMedical;
using MongoDB.Bson;
using MongoDB.Driver;

namespace GenericBackend.Controllers
{
    [AllowAnonymous]
    [RoutePrefix("api/machines")]
    public class MachineController : ApiController
    {
        private readonly ILogger _logger = NlogLogger.Instance;
        private readonly IMongoRepository<Machine> _machinesRepository;
        private readonly IMongoRepository<FullRentCustomer> _rentsRepository;
        private readonly IMongoRepository<RentProgram> _rentProgramsRepository;
        private readonly bool _isPayment; 

        public MachineController(IUnitOfWork unitOfWork)
        {
            _machinesRepository = unitOfWork.Machines;
            _rentsRepository = unitOfWork.FullRentCustomers;
            _rentProgramsRepository = unitOfWork.RentPrograms;
            var settingsRepository = unitOfWork.Settings;

            var paymentParam = settingsRepository.FirstOrDefault(x => x.ParamKey == "IsPayment");

            _isPayment = paymentParam != null && Convert.ToBoolean(paymentParam.ParamValue);
        }

        [HttpGet]
        [Route("catalog")]
        public async Task<IHttpActionResult> GetMachinePrograms()
        {
            var rentPrograms = await _rentProgramsRepository.GetList();
            var models = new List<CatalogModel>();

            foreach (var rentProgram in rentPrograms)
            {
                models.Add(new CatalogModel
                {
                    Title = rentProgram.Title,
                    Description = rentProgram.CatalogBriefInfo.CatalogDescription,
                    Ships = rentProgram.CatalogBriefInfo.Ships,
                    Features = rentProgram.CatalogBriefInfo.CatalogFeatures,
                    Type = rentProgram.Type.ToString(),
                    ImageUrl = rentProgram.ImageUrl,
                    Price = rentProgram.Price,
                    IsPayment = _isPayment
                });
            }

            return Ok(models);
        }

        [HttpGet]
        [Route("")]
        public async Task<IHttpActionResult> GetByType([FromUri]ProgramType type)
        {
            var program = _rentProgramsRepository
                .FirstOrDefault(x => x.Type == type);

            var machinesByType = await _machinesRepository.Collection
                .Find(x => x.Type == type)
                .ToListAsync();
            
            return Ok(new MachineProgramModel
            {
              Program  = program,
              Machines = machinesByType,
              IsPayment = _isPayment
            });
        }

        [HttpGet]
        [Route("{id}")]
        public IHttpActionResult Get(string id)
        {
            var machine = _machinesRepository.GetById(id);

            return Ok(machine);
        }

        [HttpGet]
        [Route("random")]
        public async Task<IHttpActionResult> GetRandom()
        {
            var random = await _machinesRepository
                .Collection
                .CountAsync(FilterDefinition<Machine>.Empty);

            var number = new Random();

            var randomMachine = _machinesRepository
                .Skip(number.Next(1, Convert.ToInt32(random)))
                .Take(1)
                .FirstOrDefault();

            var ships = _rentProgramsRepository
                .Where(x => x.Type == randomMachine.Type)
                .Select(x => x.CatalogBriefInfo.Ships)
                .FirstOrDefault();

            var model = Mapper.Map<PreviewMachineModel>(randomMachine);

            model.Ships = ships;
            model.IsPayment = _isPayment;

            return Ok(model);
        }

        [HttpGet]
        [Route("rents/seen/{id}")]
        public IHttpActionResult SetSeen(string id)
        {
            var customer = _rentsRepository.GetById(id);

            if (customer == null) return Ok();

            customer.New = false;
            _rentsRepository.Update(customer);

            return Ok();
        }

        [HttpGet]
        [Route("rents")]
        public async Task<IHttpActionResult> GetRentAsks()
        {
            var rents = await _rentsRepository.Collection.Find(new BsonDocument()).ToListAsync();

            return Ok(rents);
        }
    }
}
