﻿
using GenericBackend.DataModels.GoodNightMedical;

namespace GenericBackend.Repository.GoodNightMedical
{
    public class AnalyticsInfoRepository : MongoRepository<AnalyticsInfoItem>, IMongoRepository<AnalyticsInfoItem>
    {
    }
}
