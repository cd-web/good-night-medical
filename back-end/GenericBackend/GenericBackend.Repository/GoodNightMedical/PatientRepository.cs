﻿using GenericBackend.DataModels.GoodNightMedical;
using GenericBackend.DataModels.GoodNightMedical.PatientEntities;

namespace GenericBackend.Repository.GoodNightMedical
{
    public class PatientRepository : MongoRepository<Patient>, IMongoRepository<Patient>
    {

    }
}
