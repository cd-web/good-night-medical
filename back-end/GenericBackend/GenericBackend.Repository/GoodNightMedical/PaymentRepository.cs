﻿using GenericBackend.DataModels.GoodNightMedical;

namespace GenericBackend.Repository.GoodNightMedical
{
    public class PaymentRepository : MongoRepository<Transaction>, IMongoRepository<Transaction>
    {
    }
}
