﻿namespace GenericBackend.UnitOfWork.Tests.Infrastructure
{
    internal class DatabaseInfrastructure
    {
        private const string DatabaseUrl = "mongodb://localhost/";

        public static string GetDatabasePath<T>() where T : class 
        {
            return DatabaseUrl + typeof(T).Name;
        }
    }
}
