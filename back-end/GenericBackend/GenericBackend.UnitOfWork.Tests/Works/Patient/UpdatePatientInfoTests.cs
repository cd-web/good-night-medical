﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GenericBackend.DataModels.GoodNightMedical;
using GenericBackend.DataModels.GoodNightMedical.PatientEntities;
using GenericBackend.Repository.GoodNightMedical;
using GenericBackend.UnitOfWork.GoodNightMedical.Models.Patient;
using GenericBackend.UnitOfWork.GoodNightMedical.Works;
using NUnit.Framework;

namespace GenericBackend.UnitOfWork.Tests.Works.Patient
{
    [TestFixture]
    public class UpdatePatientInfoTests
    {
        [Test]
        public async Task NewPatientAddedWithProperData()
        {
            //given
            var patientWork = new PatientWork();
            var patient = new DataModels.GoodNightMedical.PatientEntities.Patient
            {
                Email = "someemail@mail.com",
                PhoneNumber = "+380963374456",
                DateOfBirth = new DateTime(2015, 10, 10).Date,
                FullName = "John Doe",
                Address = new List<Address>
                {
                  new Address
                  {
                      PlainAddress = "Home",
                      Preferrable = true 
                  }  
                }
            };

            //when
            patientWork.UpdatePatientInfo(patient);

            //then
            var repository = new PatientRepository();

            var dataInDb = await repository.GetList();

            Assert.That(dataInDb.Any(), Is.True);

            var last = (await repository.GetList()).Last();

            Assert.That(last.Email, Is.EqualTo(last.Email));
        }
    }
}
