﻿using GenericBackend.Logging;
using GenericBackend.Logging.Interfaces;
using GenericBackend.PaymentProcessor.Core.Interfaces;
using GenericBackend.PaymentProcessor.Models;
using GenericBackend.PaymentProcessor.Models.Payment;

namespace GenericBackend.PaymentProcessor
{
    public abstract class BaseProcessor : IPaymentProcessor
    {
        protected readonly int DefaultQuantity = 1;
        protected readonly ILogger Logger = NlogLogger.Instance;

        public abstract void SetCreditCard(CreditCardModel creditCardModelModel);
        public abstract void SetShippingBillingAddress(ShipmentModel shipmentModel, ShipmentModel billingModel, string email);
        public abstract TransactionModel InitializeChargeRequestAndExecute(string itemOrTypeName, string shipping, string itemPrice, string total);
    }
}
