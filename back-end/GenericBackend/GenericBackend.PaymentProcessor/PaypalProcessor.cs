﻿using System;
using System.Collections.Generic;
using CreditCardValidator;
using GenericBackend.PaymentProcessor.Models;
using GenericBackend.PaymentProcessor.Models.Payment;
using JetBrains.Annotations;
using PayPal;
using PayPal.Api;

namespace GenericBackend.PaymentProcessor
{
    public class PaypalProcessor : BaseProcessor
    {
        private ShippingAddress _shippingAddress;
        private Address _billingAddress;
        private CreditCard _creditCard;
        private PayerInfo _payerInfo;
        private readonly APIContext _apiContext;

        private string _currency = "USD";
        private string _defaultCountryCode = "US";
        private string _paymentMethod = "credit_card";
        private string _paymentIntent = "sale";

        public PaypalProcessor()
        {
            var config = ConfigManager.Instance.GetProperties();
            var accessToken = new OAuthTokenCredential(config).GetAccessToken();
            _apiContext = new APIContext(accessToken);
        }

        public override void SetCreditCard([NotNull]CreditCardModel creditCardModelModel)
        {
            CreditCardDetector detector = new CreditCardDetector(creditCardModelModel.CardNumber);

            _creditCard = new CreditCard
            {
                number = creditCardModelModel.CardNumber,
                expire_year = Convert.ToInt32(creditCardModelModel.ExpirationYear),
                expire_month = Convert.ToInt32(creditCardModelModel.ExpirationMonth),
                cvv2 = creditCardModelModel.Cvv,
                type = detector.BrandName.ToLowerInvariant()
            };
        }

        public override void SetShippingBillingAddress([NotNull]ShipmentModel shipmentModel, [NotNull]ShipmentModel billingModel, [NotNull]string email)
        {
            _billingAddress = new Address
            {
                line1 = billingModel.PrimaryAddress,
                line2 = billingModel.SecondaryAddress,
                city = billingModel.City,
                state = billingModel.State,
                postal_code = billingModel.Zip,
                country_code = _defaultCountryCode
            };

            _shippingAddress = new ShippingAddress
            {
                line1 = shipmentModel.PrimaryAddress,
                line2 = shipmentModel.SecondaryAddress,
                city = shipmentModel.City,
                state = shipmentModel.State,
                postal_code = shipmentModel.Zip,
                country_code = _defaultCountryCode
            };

            _creditCard.billing_address = _billingAddress;

            _payerInfo = new PayerInfo
            {
                shipping_address = _shippingAddress,
                billing_address = _billingAddress,
                first_name = shipmentModel.FirstName,
                last_name = shipmentModel.LastName,
                email = email
            };
        }

        public override TransactionModel InitializeChargeRequestAndExecute(string itemOrTypeName, string shipping, string itemPrice, string total)
        {
            List<Transaction> transactions = ConvertToItemTransactionList(itemOrTypeName, shipping, itemPrice, total);
            FundingInstrument fundInstrument = new FundingInstrument {credit_card = _creditCard};
            List<FundingInstrument> fundingInstrumentList = new List<FundingInstrument> {fundInstrument};

            var result = Execute(fundingInstrumentList, transactions);

            return result;
        }

        private TransactionModel Execute(List<FundingInstrument> fundingInstrumentList, List<Transaction> transactions)
        {
            var payment = ConvertToPaymentModel(fundingInstrumentList, transactions);
            
            try
            {
                Payment createdPayment = payment.Create(_apiContext);

                var model = new TransactionModel
                {
                    TransactionId = createdPayment.id,
                    StatusCode = createdPayment.state
                };

                return model;
            }

            catch (PayPalException ex)
            {
                var exceptionModel = new TransactionModel {StatusCode = ex.Message};

                if (ex.InnerException is PayPal.ConnectionException)
                {
                    exceptionModel.Error = ex.InnerException.Message;

                    Logger.Fatal(ex.InnerException);
                }
                else
                {
                    exceptionModel.Error = ex.Message;

                    Logger.Fatal(ex.Message);
                }

                return exceptionModel;
            }
        }

        private Payment ConvertToPaymentModel(List<FundingInstrument> list, List<Transaction> transactions)
        {
            Payer payer = new Payer
            {
                funding_instruments = list,
                payment_method = _paymentMethod,
                payer_info = _payerInfo
            };

            Payment payment = new Payment
            {
                intent = _paymentIntent,
                payer = payer,
                transactions = transactions
            };

            return payment;
        }

        private List<Transaction> ConvertToItemTransactionList(string itemOrTypeName, string shipping, string itemPrice, string total)
        {
            Details details = new Details
            {
                tax = "0",
                shipping = shipping,
                subtotal = itemPrice
            };

            Amount amont = new Amount
            {
                currency = _currency,
                total = total,
                details = details
            };

            Item item = new Item
            {
                name = itemOrTypeName,
                currency = _currency,
                price = itemPrice,
                quantity = Convert.ToString(DefaultQuantity)
            };

            List<Item> itms = new List<Item> { item };
            ItemList itemList = new ItemList { items = itms };

            Transaction tran = new Transaction
            {
                amount = amont,
                description = itemOrTypeName,
                item_list = itemList
            };

            List<Transaction> transactions = new List<Transaction> { tran };

            return transactions;
        }
    }
}
