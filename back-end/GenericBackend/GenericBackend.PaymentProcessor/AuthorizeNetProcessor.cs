﻿using System;
using System.Configuration;
using System.Diagnostics;
using AuthorizeNet.Api.Contracts.V1;
using AuthorizeNet.Api.Controllers;
using AuthorizeNet.Api.Controllers.Bases;
using GenericBackend.Logging;
using GenericBackend.Logging.Interfaces;
using GenericBackend.PaymentProcessor.Core.Interfaces;
using GenericBackend.PaymentProcessor.Models;
using GenericBackend.PaymentProcessor.Models.Payment;
using JetBrains.Annotations;

namespace GenericBackend.PaymentProcessor
{
    public class AuthorizeNetProcessor : IPaymentProcessor
    {
        private readonly string _apiLogin = ConfigurationManager.AppSettings["AuthorizeApi"]; 
        private readonly string _transactionKey = ConfigurationManager.AppSettings["TransactionKey"];
        private readonly int _defaultQuantity = 1;
        private customerAddressType _billingAddress;
        private nameAndAddressType _shippingAddress;
        private paymentType _paymentType;
        private readonly ILogger _logger = NlogLogger.Instance;

        public AuthorizeNetProcessor()
        {
            var isSandbox = Convert.ToBoolean(ConfigurationManager.AppSettings["IsSandbox"]);

            ApiOperationBase<ANetApiRequest, ANetApiResponse>.RunEnvironment = isSandbox ? AuthorizeNet.Environment.SANDBOX : AuthorizeNet.Environment.PRODUCTION;

            ApiOperationBase<ANetApiRequest, ANetApiResponse>.MerchantAuthentication = new merchantAuthenticationType()
            {
                name = _apiLogin,
                ItemElementName = ItemChoiceType.transactionKey,
                Item = _transactionKey
            };
        }

        public void SetCreditCard([NotNull]CreditCardModel creditCardModelModel)
        {
            var creditCard = new creditCardType
            {
                cardNumber = creditCardModelModel.CardNumber,
                expirationDate = creditCardModelModel.GetShortExpirationDate(),
                cardCode = creditCardModelModel.Cvv
            };

            _paymentType = new paymentType {Item = creditCard};
        }

        public void SetShippingBillingAddress([NotNull]ShipmentModel shipmentModel, [NotNull]ShipmentModel billingModel, [NotNull]string email)
        {
            _shippingAddress = new nameAndAddressType
            {
                firstName = shipmentModel.FirstName,
                lastName = shipmentModel.LastName,
                address = shipmentModel.PrimaryAddress,
                city = shipmentModel.City,
                zip = shipmentModel.Zip,
                state = shipmentModel.State
            };

            _billingAddress = new customerAddressType
            {
                firstName = billingModel.FirstName,
                lastName = billingModel.LastName,
                address = billingModel.PrimaryAddress,
                city = billingModel.City,
                zip = billingModel.Zip,
                state = billingModel.State
            };
        }

        public TransactionModel InitializeChargeRequestAndExecute(string itemOrTypeName, string shipping, string itemPrice, string total)
        {
            var lineItems = new lineItemType[_defaultQuantity];

            lineItems[0] = new lineItemType { itemId = "1", name = "Test", description = itemOrTypeName, quantity = _defaultQuantity, unitPrice = Convert.ToDecimal(itemPrice) };

            var transactionRequest = new transactionRequestType
            {
                transactionType = transactionTypeEnum.authCaptureTransaction.ToString(),
               
                amount = Convert.ToDecimal(total),
                payment = _paymentType,
                billTo = _billingAddress,
                shipTo = _shippingAddress,
                lineItems = lineItems,
            };

            var result = Execute(transactionRequest);

            return result;
        }

        private TransactionModel Execute(transactionRequestType transactionRequest)
        {
            var request = new createTransactionRequest { transactionRequest = transactionRequest };

            _logger.Info("New Transaction Requested Created");

           var controller = new createTransactionController(request);
            controller.Execute();

            var response = controller.GetApiResponse();
            var transaction = new TransactionModel();

            if (response != null && response.messages.resultCode == messageTypeEnum.Ok)
            {
                transaction.StatusCode = response.messages.resultCode.ToString();

                if (response.transactionResponse != null)
                {
                    transaction.AuthCode = response.transactionResponse.authCode;
                    transaction.TransactionId = response.transactionResponse.transId;
                    _logger.Info($"Success, Auth Code : {response.transactionResponse.authCode} Transaction Id: {response.transactionResponse.transId}");
                }

                return transaction;
            }
            if (response != null)
            {
                _logger.Fatal($"Error: {response.messages.message[0].code} {response.messages.message[0].text}");
                transaction.StatusCode = response.messages.message[0].code;
                transaction.Error = response.messages.message[0].text;

                if (response.transactionResponse != null)
                {
                    transaction.StatusCode = response.transactionResponse.errors[0].errorCode;
                    transaction.Error = response.transactionResponse.errors[0].errorText;
                    transaction.TransactionId = response.transactionResponse.transId;
                    transaction.AuthCode = response.transactionResponse.authCode;

                    _logger.Fatal($"Transaction Error : {response.transactionResponse.errors[0].errorCode} {response.transactionResponse.errors[0].errorText}");
                }

                return transaction;
            }

            return null;
        }
        
    }
}
