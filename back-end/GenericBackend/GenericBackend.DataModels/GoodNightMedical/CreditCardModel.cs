﻿using GenericBackend.Core;

namespace GenericBackend.DataModels.GoodNightMedical
{
   public class CreditCardModel : MongoEntityBase
    {
        public string CardNumber { get; set; }
        public string ExpirationMonth { get; set; }
        public string ExpirationYear { get; set; }
        public string Cvv { get; set; }
    }
}
