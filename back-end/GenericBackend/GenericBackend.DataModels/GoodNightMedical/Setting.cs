﻿using GenericBackend.Core;

namespace GenericBackend.DataModels.GoodNightMedical
{
    public class Setting : MongoEntityBase
    {
        public string ParamKey { get; set; }
        public string ParamName { get; set; }
        public string ParamValue { get; set; }
        public SettingType Type { get; set; }
    }

    public enum SettingType
    {
        Boolean,
        Text,
        Number
    }
}
