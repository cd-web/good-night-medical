﻿using GenericBackend.Core;

namespace GenericBackend.DataModels.GoodNightMedical.Requests
{
    public class PickCpapRequest : MongoEntityBase
    {
        public string Message { get; set; }
        public string PhysicianInformation { get; set; }

        public PatientEntities.Patient Patient { get; set; }
    }
}
