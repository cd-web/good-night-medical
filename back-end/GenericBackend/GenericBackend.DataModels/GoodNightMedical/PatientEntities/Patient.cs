﻿using System;
using System.Collections.Generic;
using GenericBackend.Core;

namespace GenericBackend.DataModels.GoodNightMedical.PatientEntities
{
    public class Patient : MongoEntityBase
    {
        public Patient()
        {
            Diagnoses = new List<Diagnose>();
            MedicalHistory = new List<Diagnose>();
            Address = new List<Address>();
            Documents = new List<Document>();
            ScreeningData = new List<ScreeningDataItem>();
            PaymentHistory = new List<PatientTransaction>();
        }

        public string Email { get; set; }
        public string FullName { get; set; }

        public Gender Gender { get; set; }
        public string PhoneNumber { get; set; }

        public DateTime DateOfBirth { get; set; }

        public List<Diagnose> Diagnoses { get; set; }
        public List<Diagnose> MedicalHistory { get; set; }

        public List<Address> Address { get; set; }
        public List<Document> Documents { get; set; }

        public List<ScreeningDataItem> ScreeningData { get; set; }
        public List<PatientTransaction> PaymentHistory { get; set; }
    }

    public class Address
    {
        public string State { get; set; }
        public string City { get; set; }
        public string ZipCode { get; set; }
        public string PlainAddress { get; set; }
        public bool Preferrable { get; set; }
    }

    public enum Gender
    {
        Male,
        Female
    };
}
