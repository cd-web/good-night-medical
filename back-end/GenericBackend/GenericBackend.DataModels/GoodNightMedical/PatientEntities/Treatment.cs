﻿using GenericBackend.Core.Enums;

namespace GenericBackend.DataModels.GoodNightMedical.PatientEntities
{
    public class Treatment
    {
        public string Manafacturer { get; set; }
        public ProgramType Type { get; set; }
        public string MachineName { get; set; }
    }
}
