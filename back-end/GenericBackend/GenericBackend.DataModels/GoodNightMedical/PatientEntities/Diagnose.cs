﻿using GenericBackend.Core;

namespace GenericBackend.DataModels.GoodNightMedical.PatientEntities
{
    public class Diagnose : MongoEntityBase
    {
        public string Description { get; set; }
        public Treatment Treatment { get; set; }
    }
}
