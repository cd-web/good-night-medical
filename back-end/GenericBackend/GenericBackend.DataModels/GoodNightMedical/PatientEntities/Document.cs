﻿using System;
using GenericBackend.Core;

namespace GenericBackend.DataModels.GoodNightMedical.PatientEntities
{
    public class Document : MongoEntityBase
    {
        public string Title { get; set; }
        public string Url { get; set; }
        public DateTime CreateDate { get; set; }
    }
}
