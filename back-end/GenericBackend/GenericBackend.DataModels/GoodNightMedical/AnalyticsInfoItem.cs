﻿using System;
using System.Collections.Generic;
using GenericBackend.Core;

namespace GenericBackend.DataModels.GoodNightMedical
{
    public class AnalyticsInfoItem : MongoEntityBase
    {
        public string LandingPageUrl { get; set; }
        public string Request { get; set; }
        public string RequestText { get; set; }
        public List<Analytics> Analytics { get; set; }
    }

    public class Analytics : MongoEntityBase
    {
        public string RequestPageUrl { get; set; }
        public DateTime RequestTime { get; set; }
        public Dictionary<string, string> QueryParameters { get; set; }
    }

}
