﻿using GenericBackend.Core;

namespace GenericBackend.DataModels.GoodNightMedical
{
    public class ScreeningDataItem : MongoEntityBase
    {
        public string Question { get; set; }
        public string Answer { get; set; }
    }
}
