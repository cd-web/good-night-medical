﻿using GenericBackend.Core;
using MongoDB.Bson.Serialization.Attributes;

namespace GenericBackend.DataModels.GoodNightMedical
{
    [BsonIgnoreExtraElements]
    public class Customer : MongoEntityBase, IMongoEntity
    {
        public string FullName { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public bool New { get; set; } = true;

    }
}
