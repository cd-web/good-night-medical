﻿using GenericBackend.Core;

namespace GenericBackend.DataModels.GoodNightMedical
{
    public class Transaction: MongoEntityBase
    {
        public string TransactionId { get; set; }
        public string AuthCode { get; set; }
        public string StatusCode { get; set; }
        public string Error { get; set; }
    }
}
