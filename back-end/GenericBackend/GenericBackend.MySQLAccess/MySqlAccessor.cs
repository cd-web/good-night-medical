﻿using System;
using System.Data;
using GenericBackend.DataModels.GoodNightMedical;
using MySql.Data.MySqlClient;

namespace GenericBackend.MySqlAccess
{
    public class MySqlAccessor
    {
        private const string ConnectionString = "Server=sql5.freemysqlhosting.net;Database=sql5124947;Uid=sql5124947;Pwd=rdyRfVfy8b;";
        private  MySqlConnection _connection;
        

        public void Add(AnalyticsInfoItem info)
        {
            using (_connection = new MySqlConnection(ConnectionString))
            {
                _connection.Open();

                // Check if landing page already exist in base
                var landingPagesCount = 0;
                var commandCheck = _connection.CreateCommand();
                commandCheck.CommandText =
                    "SELECT COUNT(*) FROM AnalyticsInfo WHERE LandingPageUrl=@LandingPageUrl";
                commandCheck.Parameters.AddWithValue("@LandingPageUrl", info.LandingPageUrl);
                var resultCheck = commandCheck.ExecuteScalar();

                if (resultCheck != null)
                    landingPagesCount = Convert.ToInt32(resultCheck);
                //
                if (landingPagesCount == 0)
                {
                    var command = _connection.CreateCommand();
                    command.CommandText =
                        "INSERT INTO AnalyticsInfo(LandingPageUrl,Request,RequestText) VALUES(@LandingPageUrl,@Request,@RequestText)";
                    command.Parameters.AddWithValue("@LandingPageUrl", info.LandingPageUrl);
                    command.Parameters.AddWithValue("@Request", info.Request);
                    command.Parameters.AddWithValue("@RequestText", info.RequestText);
                    command.ExecuteNonQuery();
                }

                if (info.Analytics.Count > 0)
                {
                    var analyticsInfoId = -1;
                    var command0 = _connection.CreateCommand();
                    command0.CommandText =
                        "SELECT AnalyticsInfo_Id FROM AnalyticsInfo WHERE LandingPageUrl=@LandingPageUrl";
                    command0.Parameters.AddWithValue("@LandingPageUrl", info.LandingPageUrl);
                    var result = command0.ExecuteScalar();

                    if (result != null)
                        analyticsInfoId = Convert.ToInt32(result);

                    if (analyticsInfoId > -1)
                    {
                        foreach (var item in info.Analytics)
                        {
                            var command1 = _connection.CreateCommand();
                            command1.CommandText =
                                "INSERT INTO Analytics(RequestPageUrl,RequestTime,AnalyticsInfo_Id) VALUES( @RequestPageUrl,@RequestTime,@AnalyticsInfo_Id)";
                            command1.Parameters.AddWithValue("@RequestPageUrl", item.RequestPageUrl);
                            command1.Parameters.AddWithValue("@RequestTime", item.RequestTime);
                            command1.Parameters.AddWithValue("@AnalyticsInfo_Id", analyticsInfoId);
                            command1.ExecuteNonQuery();
                            if (item.QueryParameters != null)
                            {
                                var analyticsId = -1;
                                var command2 = _connection.CreateCommand();
                                command2.CommandText =
                                    "SELECT Analytics_Id FROM Analytics WHERE AnalyticsInfo_Id=(SELECT AnalyticsInfo_Id FROM AnalyticsInfo WHERE LandingPageUrl=@LandingPageUrl)";
                                command2.Parameters.AddWithValue("@LandingPageUrl", info.LandingPageUrl);
                                var result1 = command2.ExecuteScalar();

                                if (result1 != null)
                                    analyticsId = Convert.ToInt32(result1);
                                foreach (var param in item.QueryParameters)
                                {
                                    var command3 = _connection.CreateCommand();
                                    command3.CommandText =
                                        "INSERT INTO QueryParameters(Key,Value,Analytics_Id) VALUES(@Key,@Value,@Analytics_Id)";
                                    command3.Parameters.AddWithValue("@Key", param.Key);
                                    command3.Parameters.AddWithValue("@Value", param.Value);
                                    command3.Parameters.AddWithValue("@Analytics_Id", analyticsId);
                                    command3.ExecuteNonQuery();
                                }
                            }
                        }
                    }
                }
                if (_connection.State == ConnectionState.Open)
                    _connection.Close();
            }
        }

        public AnalyticsInfoItem Get ()
        {
            return null;
        }


        public void CreateTables()
        {
            
        }
    }
}
