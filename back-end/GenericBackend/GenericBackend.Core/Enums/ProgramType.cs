﻿namespace GenericBackend.Core.Enums
{
    public enum ProgramType : short
    {
        New = 0,
        Refurbished,
        Bipap,
        Other
    }
}