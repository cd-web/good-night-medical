﻿    namespace GenericBackend.Core.Enums
{
    public enum ContactMethod
    {
        Phone,
        Email
    }
}