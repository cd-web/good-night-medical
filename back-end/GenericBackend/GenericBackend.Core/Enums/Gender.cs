﻿namespace GenericBackend.Core.Enums
{
    public enum Gender
    {
        Male,
        Female
    };
}