﻿namespace GenericBackend.Core.Enums
{
    public enum DoctorPrescription
    {
        Unsure,
        Yes,
        No,
    }
}