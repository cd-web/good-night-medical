﻿namespace GenericBackend.PaymentProcessor.Models.Payment
{
    public class TransactionModel
    {
        public string TransactionId { get; set; }
        public string AuthCode { get; set; }
        public string StatusCode { get; set; }
        public string Error { get; set; }
    }
}
