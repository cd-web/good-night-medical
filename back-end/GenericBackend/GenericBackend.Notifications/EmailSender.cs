﻿using System;
using System.Configuration;
using System.Net;
using System.Net.Mail;
using GenericBackend.Logging;
using GenericBackend.Logging.Interfaces;
using GenericBackend.Notifications.Template;

namespace GenericBackend.Notifications
{
    public class EmailSender
    {
        private SmtpClient _smtpClient;
        private readonly string _email;
        private readonly string _password;
        private readonly string _serverName;
        private readonly int _serverPort;
        private readonly bool _defaultCredentials;
        private readonly bool _useSsl;
        private readonly ILogger _logger = NlogLogger.Instance;

        public EmailSender()
        {
            _email = ConfigurationManager.AppSettings["Email"];
            _password = ConfigurationManager.AppSettings["Password"];
            _serverName = ConfigurationManager.AppSettings["Host"];
            _serverPort = Convert.ToInt32(ConfigurationManager.AppSettings["Port"]);
            _defaultCredentials = Convert.ToBoolean(ConfigurationManager.AppSettings["DefaultCredentials"]);
            _useSsl = Convert.ToBoolean(ConfigurationManager.AppSettings["EnableSSL"]);
        }

        public void Initialize()
        {
            _smtpClient = new SmtpClient(_serverName, _serverPort)
            {
                UseDefaultCredentials = _defaultCredentials
            };

            if (!_defaultCredentials)
                _smtpClient.Credentials = new NetworkCredential(_email, _password);

            _smtpClient.EnableSsl = _useSsl;
        }

        public void SendMessage(EmailModel model, bool addAttachment = false)
        {
            var emailMessage = CreateEmailMessage(model, addAttachment);

            try
            {
                _smtpClient.Send(emailMessage);

                _logger.Info($"Successfully sent message to {emailMessage.To}");
            }
            catch (Exception e)
            {
                _logger.Error("Email was not sended", e);

                throw;
            }
        }

        protected virtual MailMessage CreateEmailMessage(EmailModel model, bool addAttachment)
        {
            var mail = new MailMessage
            {
                From = new MailAddress(_email, _email),
                To = { model.To },
                Subject = model.Subject,
                Body = model.Body,
                IsBodyHtml = true
            };

            if(addAttachment)
                mail.Attachments.Add(new Attachment("test"));

            return mail;
        }
    }
}
