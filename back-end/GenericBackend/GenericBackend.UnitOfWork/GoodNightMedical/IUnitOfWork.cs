﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GenericBackend.DataModels.GoodNightMedical;
using GenericBackend.DataModels.GoodNightMedical.PatientEntities;
using GenericBackend.Repository;
using GenericBackend.UnitOfWork.GoodNightMedical.Models.Patient;

namespace GenericBackend.UnitOfWork.GoodNightMedical
{
    public interface IUnitOfWork
    {
        IMongoRepository<Machine> Machines { get; }
        IMongoRepository<RentOption> RentOptions { get; }
        IMongoRepository<MachineOrder> MachineOrders { get; }
        IMongoRepository<Customer> Customers { get; } 
        IMongoRepository<FullRentCustomer> FullRentCustomers { get; } 
        IMongoRepository<RentProgram> RentPrograms { get; } 
        IMongoRepository<Setting> Settings { get; }
        IMongoRepository<Transaction> Payment { get; }
        IMongoRepository<ReSupplyCustomer> ReSupplyCustomer { get; }
        IMongoRepository<Patient> Patient { get; }
        IMongoRepository<AnalyticsInfoItem> AnalyticsInfo { get; }
    }
}
