﻿using GenericBackend.DataModels.GoodNightMedical;
using GenericBackend.DataModels.GoodNightMedical.PatientEntities;
using GenericBackend.DataModels.GoodNightMedical.Requests;
using GenericBackend.Repository;
using GenericBackend.UnitOfWork.GoodNightMedical.Models.Patient;

namespace GenericBackend.UnitOfWork.GoodNightMedical.Interfaces
{
    public interface IPatientWork : IUnitOfWork
    {
        IMongoRepository<Patient> Patients { get; }
        IMongoRepository<PickCpapRequest> PickCpapRequests { get; }

        string UpdatePatientInfo(Patient patient);
    }
}
