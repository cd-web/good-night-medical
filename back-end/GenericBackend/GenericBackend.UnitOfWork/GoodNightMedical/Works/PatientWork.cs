﻿using GenericBackend.DataModels.GoodNightMedical;
using GenericBackend.DataModels.GoodNightMedical.PatientEntities;
using GenericBackend.DataModels.GoodNightMedical.Requests;
using GenericBackend.Repository;
using GenericBackend.Repository.GoodNightMedical;
using GenericBackend.UnitOfWork.GoodNightMedical.Interfaces;
using GenericBackend.UnitOfWork.GoodNightMedical.Models.Patient;

namespace GenericBackend.UnitOfWork.GoodNightMedical.Works
{
    public class PatientWork : UnitOfWork, IPatientWork
    {
        private readonly PatientRepository _patients;
        private readonly HelpPickCpapRepository _cpapRequests;

        public PatientWork()
        {
            _patients = new PatientRepository();
            _cpapRequests = new HelpPickCpapRepository();
        }

        public IMongoRepository<Patient> Patients => _patients;
        public IMongoRepository<PickCpapRequest> PickCpapRequests => _cpapRequests;

        public string UpdatePatientInfo(Patient patient)
        {
            _patients.Add(patient);

            return patient.Id;
        }
    }
}
