Обязательные:
- FullName
- Gender        (нету)
- Email
- PhoneNubmer
- DateOfBirth
- Address
- ScreeningData (нету) (вопрос - ответ, на анкету Sleep Apnea Screening Tool)

Опциональные (в планах, пока не известно какие точно будут):
- Height (в дюймах)
- Weight (в фунтах)

- Data1   (начал    пользоваться нашими услугами)
- Data2   (перестал пользоваться нашими услугами)
- WhyLeft (причина по которой перестал пользоваться нашими услугами)

- Documents      (загруженные файлы, в формате pdf, doc, docx?)
- MedicalHistory (история болезней)
- ModelPicked    (какие машины арендовал, и время их аренды, с mm/dd/yyyy до mm/dd/yyyy)

Возможно в будущем:
- diagnosed with obstructive sleep (диагностировался ли на присутствие Апноэ во сне?)
- social security nubmer (номер социального страхования)
- money paid (сколько заплатил денег за все время)
- address opt (второй адрес)
