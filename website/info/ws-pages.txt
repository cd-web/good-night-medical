LIST:
- catalog (cpap)
- catalog (help)
- information (about us)
- information (locations)
- information (testimonials)

TYPE:
- fixed (cpap)
- fluid (help, about us, locations, testimonials + all)

STRUCTURE:
- ws-page
-   header (include)
-   container
-     wrapper
-       aside
-         links
-         filters
-       section
-         text
-         form
-         input

FLUID WITH ASIDE (ALL PAGES)
- [a] | [s] - minimum 768 (a = 200 + 20; s = 100% - 200 - 20;)
-     | [s] - maximum 767 (a = 100%;     s = 100%;)

FIXED WITH ASIDE (CPAP PAGE)
- [a] | [b] [b] [b] - 200 + 20 + (220 + 20 + 220 + 20 + 220) + 40 = 960
- [a] | [b] [b]     - 200 + 20 + (220 + 20 + 220)            + 40 = 720
- [a] | [b]         - 200 + 20 + (220)                       + 40 = 480
-       [b]         - 220                                    + 40 = 260


































