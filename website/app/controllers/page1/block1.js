(function () {
  'use strict';

  var controllerId = 'block1';
  angular.module('app').controller(controllerId, ['$scope', '$state', 'dataService', block1Ctrl]);

  function block1Ctrl($scope, $state, dataService) {
    $scope.content = content;
    $scope.link = link;
    $scope.contactUsInfo = {};

    $scope.submitContactUsForm = function(){
      if ($scope.contactUsForm.$valid) {
          dataService.postContactUs($scope.contactUsInfo)
            .success(function () {
              $state.go('common.formThanks',
                {
                  fullName: $scope.contactUsInfo.fullName,
                  email: $scope.contactUsInfo.email,
                  phone: $scope.contactUsInfo.phone
                });
              
              $scope.contactUsInfo = {};
              $scope.contactUsForm.$setUntouched();
              $scope.contactUsForm.$setPristine();
          });
      }
    };

    $scope.returnHeight = function() {
      return { height: window.innerHeight + 40 + 'px' };
    };
  }

  var content = {
    title: 'Think You or Your Spouse has Sleep Apnea?',
    desc1: 'Enter your contact information below and receive details on our convenient home sleep testing.',
    desc2: 'It’s simple, easy, and sent directly to your door!'
  };

  var link = {
    bind1: 'Click here',
    bind2: 'to learn more about home sleep testing',
    uiSref: 'sleepTest.info'
  };

})();

