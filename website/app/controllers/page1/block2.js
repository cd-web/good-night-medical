(function () {
  'use strict';

  var controllerId = 'block2';
  angular.module('app').controller(controllerId, ['$scope', block2Ctrl]);

  function block2Ctrl($scope) {
    $scope.content = content;
  }

  var content = {
    title1: 'check out our rental program',
    desc1: 'We are pleased to offer the lowest cost CPAP/BIPAP rental program on the market, specially designed for those who don’t want to outright buy a machine. Whether you don’t have insurance coverage, have a high deductible, or are simply looking for a second machine, Good Night Medical’s rental program is for you!',
    imgUrl1: 'reason-1.jpg',
    title2: 'automatic resupply program for your convenience',
    desc2: 'Our Automatic Re­-Supply program will take your mind off the details while keeping you freshly stocked with replacement supplies delivered directly to your door. All you have to do is fill out a simple form and we will send you fresh supplies every 90 days (or as your insurance allows). We will also contact you via email or phone call, 7­-10 business days before your orders ships to let you know your supplies are coming. If your insurance or doctor has changed or you just need to make a last minute adjustment, you can call or email us at info@goodnightmedical.com',
    imgUrl2: 'reason-2.jpg'
  };

})();

