(function () {
  'use strict';

  var controllerId = 'block5';
  angular.module('app').controller(controllerId, ['$scope', 'dataService', 'shareService', block5Ctrl]);

  function block5Ctrl($scope, dataService, shareService) {
    $scope.content = {};

    populate();

    function populate() {
      dataService.getRandomMachine().success(function (data){
        $scope.content = data;
        shareService.addId($scope.content.id);
        shareService.addType($scope.content.model);
        shareService.setIsMachine(true);
      });
    }
  }

})();

