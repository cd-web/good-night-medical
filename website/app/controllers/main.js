(function () {
  'use strict';

  var controllerId = 'main';
  angular.module('app').controller(controllerId, ['$scope', mainCtrl]);

  function mainCtrl($scope) {
    $scope.pattern = pattern;
    $scope.wsPages = wsPages;

    $scope.showModal1 = false;
    $scope.showModal2 = false;
    $scope.showModal3 = false;
    $scope.showModal4 = false;

    // Возращает красную обводку элементу (валидация форм)
    $scope.returnClass = function(touched, invalid, form) {
      if ((invalid && touched) ||
          (invalid && form))
        {return 'error';}
    };

    // Показывает или скрывает модальные окна
    $scope.showModal1Func = function() {$scope.showModal1 = !$scope.showModal1;};
    $scope.showModal2Func = function() {$scope.showModal2 = !$scope.showModal2;};
    $scope.showModal3Func = function() {$scope.showModal3 = !$scope.showModal3;};
    $scope.showModal4Func = function() {$scope.showModal4 = !$scope.showModal4;};
  };

  // Шаблоны для разных элементов формы
  var pattern = {
    email: /^(([a-zA-Z]|[0-9])|([-]|[_]|[.]))+[@](([a-zA-Z0-9])|([-])){2,63}([.]+([a-zA-Z0-9]){2,63})+$/,
    phone: /^(([\d])|([+]|[ ]|[.]|[-]|[(]|[)]))+$/,
    name: /^([a-zA-Z]|[ ])+$/,
    cvv: /^([\d]{3,4})+$/,
    zip: /^([\d]{5})+$/
  };

  var wsPages = [{
    bind: 'About Us',
    sref: 'information.aboutUs'
  },{
    bind: 'Locations',
    sref: 'information.locations'
  },{
    bind: 'Testimonials',
    sref: 'information.testimonials'
  },{
    bind: 'Sleep Apnea Screening Tool',
    sref: 'information.screeningTool'
  }];

})();

