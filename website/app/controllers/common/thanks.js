(function () {
  'use strict';

  var controllerId = 'thanks';
  angular.module('app').controller(controllerId, ['$scope', '$state', '$window', thanksCtrl]);

  function thanksCtrl($scope, $state, $window) {
    $scope.content = [];
    
    $scope.populate = function () {
      $scope.shipment = $state.params.shipment;
      $scope.shipment.title = 'Shipping';
      $scope.billing = $state.params.billing;
      $scope.billing.title = 'Billing';

      $scope.content.push($scope.shipment);
      $scope.content.push($scope.billing);

      $window.google_trackConversion({
        google_conversion_id: 958510283,
        google_conversion_label: 'bI9bCO_upGoQy-mGyQM',
        google_conversion_language: "en",
        google_conversion_format: "3",
        google_conversion_color: "ffffff",
        google_conversion_value: 1.00,
        google_conversion_currency: "USD",
        google_remarketing_only: false
      });
    };
  }
})();

