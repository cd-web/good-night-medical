(function () {
  'use strict';

  var controllerId = 'formThanks';
  angular.module('app').controller(controllerId, ['$scope', '$state', '$window', thanksCtrl]);

  function thanksCtrl($scope, $state, $window) {

    $scope.populate = function () {
      $scope.fullName = $state.params.fullName;
      $scope.email = $state.params.email;
      $scope.phone = $state.params.phone;
      $scope.comments = $state.params.comments;

      $window.google_trackConversion({
        google_conversion_id: 958510283,
        google_conversion_language: "en",
        google_conversion_format: "3",
        google_conversion_color: "ffffff",
        google_conversion_label: "AlaHCLfppGoQy-mGyQM",
        google_remarketing_only: false
      });
    };
  }
})();

