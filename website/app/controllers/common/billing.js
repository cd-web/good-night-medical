(function () {
  'use strict';

  var controllerId = 'billing';
  angular.module('app').controller(controllerId, ['$scope', '$state', 'dataService', billingCtrl]);

  function billingCtrl($scope, $state, dataService) {
    $scope.months = months;
    $scope.years = years;
    $scope.states = states;
    $scope.purchaseResult = purchaseResult;
    $scope.purchaseInfo = purchaseInfo;
    $scope.shipData = {};

    $scope.shipData.isSameBillShip = true;
    $scope.shipData.type = $state.params.type;

    purchaseResultTotal();

    $scope.submitPurchase = function(){
      if ($scope.billData.$valid) {

        dataService.postPayment($scope.shipData).success(function (data) {
          $scope.content = data;
          if($scope.shipData.isSameBillShip){
            $state.go('common.thanks', {shipment: $scope.shipData.shipment, billing: $scope.shipData.shipment });
          }else{
            $state.go('common.thanks', {shipment: $scope.shipData.shipment, billing: $scope.shipData.billing });
          }
        });
      }
    };

    function purchaseResultTotal() {
      $scope.shipData.total = $state.params.price;
      $scope.shipData.itemPrice = $state.params.price;

      if($state.params.ships.length > 0)
      {
        $scope.shipData.shipping = $state.params.ships;
        $scope.shipData.total = parseInt($scope.shipData.itemPrice) + parseInt($scope.shipData.shipping);
      }
    }
  }

  var purchaseResult = [{
    bind: 'Sub Total',
    cost: 75
  },{
    bind: 'Tax',
    cost: 1.12
  }];

  var months = ['01','02','03','04','05','06','07','08','09','10','11','12'];
  var years  = [];
  for (var i = 0; i < 21; i++) years.push(new Date().getFullYear() + i);

  var purchaseInfo = '*By clicking “Purchase” you understand that your insurance and/or prescription still need to be approved. If for any reason we are not able to process your order due to insurance or lack of valid prescription, we will refund your money in full.';

  var states = ["Alabama", "Alaska", "Arizona", "Arkansas", "California", "Colorado", "Connecticut", "Delaware", "Florida", "Georgia", "Hawaii", "Idaho", "Illinois", "Indiana", "Iowa", "Kansas", "Kentucky", "Louisiana", "Maine", "Maryland", "Massachusetts", "Michigan", "Minnesota", "Mississippi", "Missouri", "Montana", "Nebraska", "Nevada", "New Hampshire", "New Jersey", "New Mexico", "New York", "North Carolina", "North Dakota", "Ohio", "Oklahoma", "Oregon", "Pennsylvania", "Rhode Island", "South Carolina", "South Dakota", "Tennessee", "Texas", "Utah", "Vermont", "Virginia", "Washington", "West Virginia", "Wisconsin", "Wyoming"];

})();






















