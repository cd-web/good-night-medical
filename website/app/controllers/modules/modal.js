(function() {
  'use strict';

  var controllerId = 'modal';
  angular.module('app').controller(controllerId, ['$state', '$scope', 'dataService', 'shareService', modalCtrl]);

  function modalCtrl($state, $scope, dataService, shareService) {
    $scope.errorMsg = errorMsg;
    $scope.modal2Msg = modal2Msg;
    $scope.dataList = dataList;
    $scope.model = {};
    $scope.model.prescription = 'No';
    $scope.sleepTestModel = {};

    // Отправляет данные, если все поля заполнены верно (функция вызывается с помощью ng-submit)
    $scope.submitModalForm1 = function(){
      if ($scope.modalForm.$valid) {
          if(shareService.isMachine()) {
            $scope.model.machineId = shareService.getId();
          } else {
            $scope.model.program = shareService.getType();
          }

          $scope.model.origin = $state.current.name;

          dataService.postRent($scope.model).success(function () {

            $state.go('common.formThanks',
              {
              fullName: $scope.model.fullName,
              email: $scope.model.email,
              phone: $scope.model.phone, 
              comments: $scope.model.comments
              });

            $scope.model = {};
            $scope.model.prescription = 'No';
            $scope.modalForm.$setUntouched();
            $scope.modalForm.$setPristine();
          });

          $scope.showModal1Func();
      }
    };

    $scope.clearForm2 = function () {
      $scope.sleepTestModel = {};
      $scope.modalForm2.$setUntouched();
      $scope.modalForm2.$setPristine();
    };
    
    $scope.submitModalForm4 = function () {
      if($scope.modalForm2.$valid) {
        dataService.postHelpTestRequest($scope.sleepTestModel).success(function(){
          $scope.sleepTestModel = {};
          $scope.modalForm2.$setUntouched();
          $scope.modalForm2.$setPristine();
        })

        $scope.showModal4Func();
      }
    }
  }

  var errorMsg = {
    bind: 'Required fields are not filled'
  };

  var modal2Msg = 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cupiditate aliquid magnam nam quisquam quasi vitae quidem omnis minus.';

  var dataList = [{
    bind1: 'Title',
    bind2: 'Voluptatum provident ratione sed consequatur optio tempora, rem adipisci nihil, id at modi ducimus a perferendis necessitatibus?'
  },{
    bind1: 'Title',
    bind2: 'Aliquam quae at voluptatum dolorum voluptatibus molestias ab assumenda veritatis repellendus sunt inventore incidunt nisi illo, eum voluptate maiores maxime, delectus saepe omnis? Quaerat, optio.'
  },{
    bind1: 'Title',
    bind2: 'Aliquid dignissimos cumque eum quas ea similique? Hic, quasi, ex? Provident hic, molestiae impedit at assumenda veniam. Dolore id earum fugit voluptas vel optio expedita.'
  },{
    bind1: 'Title',
    bind2: 'Quidem nobis hic assumenda, corporis natus laboriosam vel temporibus. Iure aliquam, minima autem ab totam earum magnam reprehenderit recusandae soluta iste excepturi quas repudiandae obcaecati!'
  },{
    bind1: 'Title',
    bind2: 'Porro voluptates consequuntur hic perferendis odit praesentium qui nobis, dolores in veritatis. Hic odit delectus, sapiente ex, quis, unde enim non obcaecati ad in deserunt.'
  }];

})();
