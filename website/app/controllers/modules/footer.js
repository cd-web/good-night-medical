(function () {
  'use strict';

  var controllerId = 'footer';
  angular.module('app').controller(controllerId, ['$scope', '$state', 'dataService', footerCtrl]);

  function footerCtrl($scope, $state, dataService) {
    $scope.info = info;
    $scope.linksState = linksState;
    $scope.linksSocial = linksSocial;
    $scope.contactUsInfo = {};

    $scope.submitContactUsForm = function(){
      if ($scope.contactUsForm.$valid) {
        dataService.postContactUs($scope.contactUsInfo)
          .success(function () {

            $state.go('common.formThanks',
              {
                fullName: $scope.contactUsInfo.fullName,
                email: $scope.contactUsInfo.email,
                phone: $scope.contactUsInfo.phone
              });

            $scope.contactUsInfo = {};
            $scope.contactUsForm.$setUntouched();
            $scope.contactUsForm.$setPristine();
          });
      }
    };
  }

  var info = {
    aboutUs: 'Good Night Medical is proud to be nationally known for its strength and knowledge in the area of sleep medicine and home ventilation. Good Night Medical provides the required services needed to diagnose a Sleep Apnea patient in their home. Once diagnosed, Good Night Medical provides CPAP treatment via home ventilation and oxygen equipment to our patients. Good Night Medical is dedicated to saving patients on their healthcare costs. This includes fighting for their insurance coverage, offering low cost options for those without insurance, and finding ways to cut the costs of these products through innovative recycling, refurbishing, or sourcing.',
    copyright: 'Copyright © 2016 Good Night Medical. All Rights Reserved.'
  };

  var linksState = [{
      bind: 'home',
      sref: 'home'
    },{
      bind: 'about us',
      sref: 'information.aboutUs'
    },{
      bind: 'locations',
      sref: 'information.locations'
    },{
      bind: 'rent',
      sref: 'rent.catalog'
    },{
      bind: 'auto re-supply',
      sref: 'reSupply.info'
    },{
      bind: 'home sleep',
      sref: 'sleepTest.info'
    },{
      bind: 'testimonials',
      sref: 'information.testimonials'
    }];

  var linksSocial = [{
      bind: 'facebook',
      href: '//facebook.com/goodnightmed/'
    },{
      bind: 'twitter',
      href: '//twitter.com/goodnightmed'
    },{
      bind: 'pinterest',
      href: '//pinterest.com/goodnightmed/'
    }];

})();

