(function() {
  'use strict';

  var controllerId = 'testPage1';
  angular.module('app').controller(controllerId, ['$scope', 'FileUploader', testPage1Ctrl]);

  function testPage1Ctrl($scope, FileUploader) {
    $scope.states = states;
    $scope.options = options;
    $scope.uploader = new FileUploader();

    $scope.uploader.onAfterAddingAll = function () {
      if ($scope.uploader.getNotUploadedItems().length > 1) $scope.uploader.removeFromQueue(0);
    };

    $scope.setOption = function(opt) {
      $scope.testSelect = opt.title;
    };

    $scope.showOptions = function() {
      $scope.listOptions = !$scope.listOptions;
    };
  }

  var states = [
    'Alabama',
    'Alaska',
    'Arizona',
    'Arkansas',
    'California',
    'Colorado',
    'Connecticut',
    'Delaware'
  ];

  var options = [{
    title: 'Option Title 1',
    description: 'Option Text 1',
    imgUrl: 'OPT 1'
  },{
    title: 'Option Title 2',
    description: 'Option Text 2',
    imgUrl: 'OPT 2'
  },{
    title: 'Option Title 3',
    description: 'Option Text 3',
    imgUrl: 'OPT 3'
  },{
    title: 'Option Title 4',
    description: 'Option Text 4',
    imgUrl: 'OPT 4'
  },{
    title: 'Option Title 5',
    description: 'Option Text 5',
    imgUrl: 'OPT 5'
  },{
    title: 'Option Title 6',
    description: 'Option Text 6',
    imgUrl: 'OPT 6'
  },{
    title: 'Option Title 7',
    description: 'Option Text 7',
    imgUrl: 'OPT 7'
  },{
    title: 'Option Title 8',
    description: 'Option Text 8',
    imgUrl: 'OPT 8'
  },{
    title: 'Option Title 9',
    description: 'Option Text 9',
    imgUrl: 'OPT 9'
  },{
    title: 'Option Title 10',
    description: 'Option Text 10',
    imgUrl: 'OPT 10'
  }];

})();
