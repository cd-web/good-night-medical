(function () {
  'use strict';

  var controllerId = 'rentCatalog';
  angular.module('app').controller(controllerId, ['$scope', 'dataService','shareService', rentCatalogCtrl]);

  function rentCatalogCtrl($scope, dataService, shareService) {
    $scope.machines = [];
    $scope.setMachineType = setMachineType;
    
    populate();

    function populate() {
      dataService.getCatalog().success(function (data) {
        $scope.machines = data;
      });
    }
    
    function setMachineType(type) {
      shareService.setIsMachine(false);
      shareService.addType(type);
    }
    
    // Скрывает информацию о машине
    $scope.showMachineCheck = function(index) {
      if (index !== showMachine) {return 'hide-machine';}
    };

    // Если была нажата кнопка "влево"
    $scope.leftMachine = function() {
      showMachine -= 1;
      imgClass[showMachine] = 'enabled';
      imgClass[showMachine + 1] = 'disabled-right';
      $scope.disableBtn();
    };

    // Если была нажата кнопка "вправо"
    $scope.rightMachine = function() {
      showMachine += 1;
      imgClass[showMachine] = 'enabled';
      imgClass[showMachine - 1] = 'disabled-left';
      $scope.disableBtn();
    };

    // Отключаем кнопки "влево" или "вправо" если достигнут предел
    $scope.disableBtn = function() {
      (showMachine == 0) ? $scope.leftDisable = 'disable' : $scope.leftDisable = '';
      (showMachine == 2) ? $scope.rightDisable = 'disable' : $scope.rightDisable = '';
    };

    // Возращаем изображению нужный стиль
    $scope.returnImgClass = function(index) {
      return imgClass[index];
    };
  }

  var showMachine = 1;

  var imgClass = [
    'disabled-left',
    'enabled',
    'disabled-right'
  ];
})();

