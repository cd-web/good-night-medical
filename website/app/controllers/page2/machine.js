(function () {
  'use strict';

  var controllerId = 'rentMachine';
  angular.module('app').controller(controllerId, ['$scope', '$stateParams', 'dataService', 'shareService', rentMachineCtrl]);

  function rentMachineCtrl($scope, $stateParams, dataService, shareService) {

    var type = $stateParams.type;
    $scope.ships = "";

    $scope.setMachineType = function (type) {
      shareService.setIsMachine(false);
      shareService.addType(type);
    };

    
    $scope.setMachine = function (id) {
      shareService.setIsMachine(true);
      shareService.addId(id);
    };

    dataService.getMachinesByType(type).success(function (data) {
      $scope.data = data;
      $scope.ships = $scope.data.program.catalogBriefInfo.ships;
    });
  }
})();

