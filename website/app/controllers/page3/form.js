(function () {
  'use strict';

  var controllerId = 'reSupplyForm';
  angular.module('app').controller(controllerId, ['$scope', 'dataService',reSupplyFormCtrl]);

  function reSupplyFormCtrl($scope,dataService) {
    $scope.months = months;
    $scope.days = days;
    $scope.years = years;

    $scope.currentPatient = true;

    $scope.submitData = function(){
      if ($scope.reSupply.$valid) {
        var reSupplyCustomerModel = {
            firstName: $scope.firstName,
            lastName: $scope.lastName,
            email: $scope.email,
            phone: $scope.phone,
            birthDate: new Date(Date.parse($scope.month + ' ' + $scope.day + ' ' + $scope.year)),
            currentPatient: $scope.currentPatient
        };
        dataService.postReSupply(reSupplyCustomerModel).success(function () {
          $state.go('reSupply.info');
        });
      }
    };
  }

  var months = [
    'January',
    'February',
    'March',
    'April',
    'May',
    'June',
    'July',
    'August',
    'September',
    'October',
    'November',
    'December'
  ];
  var days = [];
  for (var i = 1; i < 32; i++) {days.push(i);}
  var years  = [];
  for (var i = 0; i < 120; i++) {years.push(new Date().getFullYear() - i);}

})();

