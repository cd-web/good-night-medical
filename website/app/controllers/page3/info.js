(function () {
  'use strict';

  var controllerId = 'reSupplyInfo';
  angular.module('app').controller(controllerId, ['$scope', reSupplyInfoCtrl]);

  function reSupplyInfoCtrl($scope) {
    $scope.content = content;
  }

  var content = {
    header: {
      title: 'CPAP Automatic Re-Supply - Supplies Every 3 Month',
      bind1: 'new cpap',
      bind2: 'supplies',
      bind3: 'delivered to your door!'
    },
    body: {
      title: 'what’s in the box?',
      bind1: 'Most insurance covers new supplies every 3 months, so all you need to pay is the co-pay every 90 days (or as your insurance allows)',
      bind2: 'We’ll contact you via phone or email 7-10 business days before your order ships to let you know your supplies are coming.',
      bind3: {
        title1: 'Each Auto Re-Supply',
        title2: 'Box includes:',
        list: [
          'Cushions/Pillows',
          'Tube',
          'Disposable Filters',
          'Reusable Filter',
          'Mask',
          'Chin Strap'
        ]
      }
    },
    footer: {
      bind1: 'Not sure if you have Sleep Apnea?',
      bind2: 'Click here',
      bind3: ' to learn more about how you can get a Home Sleep Test!'
    }
  };

})();

