(function () {
  'use strict';

  var controllerId = 'screeningTool';
  angular.module('app').controller(controllerId, ['$scope', '$state', 'dataService', screeningToolCtrl]);

  function screeningToolCtrl($scope, $state, dataService) {
    $scope.bmiAnswer = bmiAnswer;
    $scope.heightType = 'in';
    $scope.weightType = 'lbs';
    $scope.months = months;
    $scope.days = days;
    $scope.years = years;
    $scope.screenedPatient = {};

    $scope.submitScreeningTool = function(){
        if ($scope.sleepScreeningTool.$valid) {
            $scope.screenedPatient.fullName = $scope.firstName + " " + $scope.lastName;
            $scope.screenedPatient.dateOfBirth = new Date($scope.year, monthConverter($scope.month), $scope.day);
            $scope.screenedPatient.screeningData = [];
            for(var i=0; i<$scope.questions.length;i++)
            {
                $scope.screenedPatient.screeningData.push({
                    question: $scope.questions[i].label,
                    answer: $scope.patientAnswers[i]
                });
            }
            dataService.postScreening($scope.screenedPatient).success(function () {
              $state.go('sleepTest.info');
            });
        }
    };

    // Вычисляем BMI
    $scope.calculateBMI = function() {
      if ($scope.heightAmount && $scope.weightAmount) {
        if ($scope.heightType == 'in') {height = $scope.heightAmount * 1;}
        else                           {height = $scope.heightAmount * 0.393701;}

        if ($scope.weightType == 'lbs') {weight = $scope.weightAmount * 1;}
        else                            {weight = $scope.weightAmount * 2.20462;}

        bmi = Math.round((weight / (height * height)) * 703 * 10) / 10;

        if      (               bmi < 15)   {text = 'very severely underweight';}
        else if (  15 <= bmi && bmi < 16)   {text = 'severely underweight';}
        else if (  16 <= bmi && bmi < 18.5) {text = 'underweight';}
        else if (18.5 <= bmi && bmi < 25)   {text = 'normal';}
        else if (  25 <= bmi && bmi < 30)   {text = 'overweight';}
        else if (  30 <= bmi && bmi < 35)   {text = 'Obese Class I';}
        else if (  35 <= bmi && bmi < 40)   {text = 'Obese Class II';}
        else if (  40 <= bmi)               {text = 'Obese Class III';}

        $scope.bmiAnswer = 'Your BMI is ' + bmi + '. The National Institute of Health rating scale places you in the ' + text;
      }
      else { $scope.bmiAnswer = 'Please enter data';}
    }

    $scope.patientAnswers = [];

    $scope.questions = [{
        label: 'Do you snore?',
        answers: [
          'Yes',
          'No',
          'Dont Know'
        ]
    }, {
        label: 'If you snore, your snoring is...',
        answers: [
          'Slightly louder than breathing',
          'As loud as talking',
          'Louder than talking',
          'Very loud - can be heard in adjacent rooms'
        ]
    }, {
        label: 'How often do you snore?',
        answers: [
          'Nearly every day',
          '3-4 times a week',
          '1-2 times a week',
          '1-2 times a month',
          'Never or nearly never'
        ]
    }, {
        label: 'Has your snoring ever bothered other people?',
        answers: [
          'Yes',
          'No',
          'Dont Know'
        ]
    }, {
        label: 'Has anyone noticed that you quit breathing during your sleep?',
        answers: [
          'Nearly every day',
          '3-4 times a week',
          '1-2 times a week',
          '1-2 times a month',
          'Never or nearly never'
        ]
    }, {
        label: 'How often do you feel tired or fatigued after your sleep?',
        answers: [
          'Nearly every day',
          '3-4 times a week',
          '1-2 times a week',
          '1-2 times a month',
          'Never or nearly never'
        ]
    }, {
        label: 'During your waking time, do you feel tired, fatigued, or not up to par?',
        answers: [
          'Nearly every day',
          '3-4 times a week',
          '1-2 times a week',
          '1-2 times a month',
          'Never or nearly never'
        ]
    }, {
        label: 'Have you ever nodded off or fallen asleep while driving a vehicle?',
        answers: [
          'Yes',
          'No'
        ]
    }, {
        label: 'If yes, how often does it occur?',
        answers: [
          'Nearly every day',
          '3-4 times a week',
          '1-2 times a week',
          '1-2 times a month',
          'Never or nearly never'
        ]
    }, {
        label: 'Do you have high blood pressure?',
        answers: [
          'Yes',
          'No',
          'Dont Know'
        ]
    }];


  };

  var weight = 0;
  var height = 0;
  var bmi = 0;
  var bmiAnswer = '';
  var text = '';

  var months = [
   '01 January',
   '02 February',
   '03 March',
   '04 April',
   '05 May',
   '06 June',
   '07 July',
   '08 August',
   '09 September',
   '10 October',
   '11 November',
   '12 December'
  ];

    var monthConverter = function(month) {
        switch(month) {
            case    '01 January':
                return 1;
            case   '02 February':
                return 1;
            case    '03 March':
                return 1;
            case   '04 April':
                return 1;
            case    '05 May':
                return 1;
            case   '06 June':
                return 1;
            case    '07 July':
                return 1;
            case   '08 August':
                return 1;
            case    '09 September':
                return 1;
            case   '10 October':
                return 1;
            case   '11 November':
                return 1;
            case '12 December':
                return 1;
        }
    };
  var days = [];
  for (var i = 1; i < 32; i++) { days.push(i); }
  var years = [];
  for (var i = 0; i < 120; i++) { years.push(new Date().getFullYear() - i); }

})();

















