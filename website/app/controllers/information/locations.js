(function () {
  'use strict';

  var controllerId = 'locations';
  angular.module('app').controller(controllerId, ['$scope', locationsCtrl]);

  function locationsCtrl($scope) {
    $scope.content = content;
  };

  var content = [{
    state: 'Arkansas',
    stateShort: 'AR',
    city1: 'Benton',
    city2: 'Benton',
    address: '626 Alcoa Rd.',
    zipCode: '72015',
    openNowAM: '8:30',
    openNowPM: '5:00',
    timeZone: 'CST',
    contact: [{
      bind1: 'Office Fax',
      bind2: '855-630-9555'
    },{
      bind1: 'Office Phone',
      bind2: '844-288-0232'
    }]
  },{
    state: 'Massachusetts',
    stateShort: 'MA',
    city1: 'Winchester',
    city2: 'Winchester',
    address: '220 Swanton St.',
    zipCode: '01890',
    openNowAM: '8:00',
    openNowPM: '3:00',
    timeZone: 'EST',
    contact: [{
      bind1: 'Office Fax',
      bind2: '781-396-0237'
    },{
      bind1: 'Office Phone',
      bind2: '781-396-3683'
    },{
      bind1: 'Scheduling Phone (free)',
      bind2: '877-753-3742'
    }]
  },{
    state: 'Ohio',
    stateShort: 'OH',
    city1: 'Cleveland',
    city2: 'Middleburg Heights',
    address: '17900 Jefferson Park Rd. #102',
    zipCode: '44130',
    openNowAM: '9:00',
    openNowPM: '4:30',
    timeZone: 'EST',
    contact: [{
      bind1: 'Scheduling Phone (free)',
      bind2: '877-753-3742 ext 2453'
    }]
  },{
    state: 'Ohio',
    stateShort: 'OH',
    city1: 'Columbus',
    city2: 'Westerville',
    address: '975 Eastwind Dr. #165',
    zipCode: '43081',
    openNowAM: '10:00',
    openNowPM: '4:00',
    timeZone: 'EST',
    contact: [{
      bind1: 'Office Fax',
      bind2: '614-568-1557'
    },{
      bind1: 'Scheduling Phone (free)',
      bind2: '877-753-3742'
    }]
  },{
    state: 'Texas',
    stateShort: 'TX',
    city1: 'Austin',
    city2: 'Austin',
    address: '8906 Wall St. #801',
    zipCode: '78754',
    openNowAM: '8:30',
    openNowPM: '5:00',
    timeZone: 'CST',
    contact: [{
      bind1: 'Office Fax',
      bind2: '512-719-3634'
    },{
      bind1: 'Office Phone',
      bind2: '512-719-3285'
    }]
  },{
    state: 'Texas',
    stateShort: 'TX',
    city1: 'Dallas',
    city2: 'Dallas',
    address: '10920 Switzer Ave. #106',
    zipCode: '75238',
    openNowAM: '8:30',
    openNowPM: '4:30',
    timeZone: 'CST',
    contact: [{
      bind1: 'Office Fax',
      bind2: '214-353-9594'
    },{
      bind1: 'Office Phone',
      bind2: '214-353-9090'
    }]
  },{
    state: 'Texas',
    stateShort: 'TX',
    city1: 'Houston',
    city2: 'Houston',
    address: '6118 Westline Dr.',
    zipCode: '77036',
    openNowAM: '8:30',
    openNowPM: '5:00',
    timeZone: 'CST',
    contact: [{
      bind1: 'Office Fax',
      bind2: '713-680-1115'
    },{
      bind1: 'Office Phone',
      bind2: '713-680-1111'
    }]
  },{
    state: 'Texas',
    stateShort: 'TX',
    city1: 'San Antonio',
    city2: 'San Antonio',
    address: '10425 Gulfdale St.',
    zipCode: '78216',
    openNowAM: '8:00',
    openNowPM: '5:00',
    timeZone: 'CST',
    contact: [{
      bind1: 'Office Fax',
      bind2: '210-697-3801'
    },{
      bind1: 'Office Phone',
      bind2: '210-697-3800'
    }]
  }];

})();
