(function () {
  'use strict';

  var controllerId = 'aboutUs';
  angular.module('app').controller(controllerId, ['$scope', aboutUsCtrl]);

  function aboutUsCtrl($scope) {
    $scope.content = content;
    $scope.history = history;
    $scope.mission = mission;
  };

  var content = {
    title: '',
    desc: [
     'Good Night Medical is proud to be nationally known for its strength and knowledge in the area of sleep medicine and home ventilation. It is also known for the substantial expertise represented in its personnel. We make a significant effort to involve the patients and their families directly in their own care.',
     'Good Night Medical provides the required services needed to diagnose a Sleep Apnea patient in their home. Once diagnosed, Good Night Medical provides CPAP treatment to our patients. This includes individual mask fitting and CPAP machine care instructions by trained and licensed Respiratory Therapists, as well as self help resources to manage everything from how to adjust their mask fit to how to fix their broken CPAP/BiPAP unit.',
     'Good Night Medical also provides home Ventilation and Oxygen equipment. This includes pediatric through adult. It includes invasive and non-invasive ventilators. Our patients are seen monthly by a licensed professional. These visits often include more education for the family. We are proud that our services help patients to spend time at home rather than in an institution. This saves the family money, saves the State money, and often reduces the stress of visiting a loved one in a hospital or other primary care institution.',
     'Good Night Medical has a Medical Director who is Board-certified in sleep medicine to consult with other physicians. Good Night Medical is accredited with both the Community Health Accreditation Program (CHAP) and American Academy of Sleep Medicine (AASM).',
     'Good Night Medical had humble beginnings of taking over the care of patients who had been abandoned by their health care provider due to financial difficulties. This includes stepping in to protect Protected Health Information (PHI) and medical records – through the safeguarding of electronic copies, to collecting almost 10,000 boxes of records from over 90 locations nationwide at the request of various courts. We are proud that we are called by Courts and Ombudsmen when they need a company with strong processes to find, protect and make available to patients, their medical records, often on short notice. Some of these companies include Sleep Health Centers, Inc., Copper Creek Medical, Inc., Blue Mountain CPAP, LLC and Total Sleep, Inc. From this we have formed Good Night Medical, with a very clear understanding, that ultimately, the best caregiver is often the patient and their own family.',
     'Good Night Medical is dedicated to saving patients on their healthcare costs. This includes fighting for their insurance coverage, offering low cost options for those without insurance and finding ways to cut the costs of these products through innovative recycling, refurbishing, or sourcing.'
    ]
  };

  var history = {
    title: 'History',
    desc: [
     'Good Night Medical comes from humble beginnings. A small, but dedicated, group of professionals with backgrounds in health care, finance, insurance, and compliance who wanted to create an organization to help people with sleep disorders. Good Night Medical has arisen from the ashes of several former sleep businesses, both large and small, including sleep medicine clinics,sleep diagnostic testing facilities, durable medical equipment suppliers, and home sleep testingproviders. Good Night Medical has grown rapidly, and today has locations across the U.S., including Massachusetts, Ohio, Texas, Arizona, Arkansas and Washington. As a result of our collaborative approach to working with patients and providers, Good Night Medical has become one of the nation’s premier providers of home medical supplies and equipment, includingoxygen, ventilators, sleep apnea supplies, and home sleep testing.',
     'Good Night Medical is made up of hard-working and caring people whose main goal is to take care of patients. Our promise to you is to work hard each day, so you can have a “Good Night!”'
    ]
  };

  var mission = {
    title: 'Mission and Values',
    desc1: 'Our mission is to be the preferred provider of services that improve and enhance the health and quality of life for individuals with sleep disorders. We achieve leadership through the constant pursuit of superior levels of quality, efficiency and patient care. Our values include:',
    desc2: 'Good Night Medical vision is to provide a total solution for comprehensive patient-centered sleep medicine and management services on a national basis to consumers, payers, employers and providers.',
    list: [
      'Each customer is treated with the utmost respect and dignity.',
      'Each member of the staff is empowered to maximize their potential to provide each patient with the highest quality, comprehensive, cost effective care.',
      'The company recognizes the need to invest in the development of its staff, new technologies and the dissemination of information in a manner that supports its mission and its ability to continuously enhance the quality of services we provide.'
     ]
  };

})();
