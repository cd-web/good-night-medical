(function () {
  'use strict';

  var controllerId = 'catalogCpap';
  angular.module('app').controller(controllerId, ['$scope', 'dataService','shareService', catalogCpapCtrl]);

  function catalogCpapCtrl($scope, dataService, shareService) {
    $scope.range = {
      name: 'Price', // Заголовок ползунка
      minCost: 0,    // Мин. выбранная цена
      maxCost: 75,   // Мак. выбранная цена
      min: 0,        // Мин. возможная цена
      max: 75        // Мак. возможная цена
    };

    $scope.isRefurbished = false;
    $scope.isNew = false;
    
    $scope.filters = filters;
    $scope.links = links;

    $scope.machines = [];
    $scope.includedCompanies = [];

    $scope.checkState = checkState;
    $scope.includeCompany = includeCompany;
    $scope.setMachine = setMachine;
    
    populate();
    
    function populate() {
      dataService.getMachines().success(function (data){
        $scope.machines = data;
      })
    }
    
    function setMachine(id) {
      shareService.setIsMachine(true);
      shareService.addId(id);
    }
    
    function checkState(name, reversedState) {
      if(name == 'New') {
        if($scope.isNew) {
          $scope.isRefurbished = reversedState;
        }
      }else {
        if($scope.isRefurbished){
          $scope.isNew = reversedState;
        }
      }
    }

     function includeCompany(company) {
      var i = $.inArray(company,  $scope.includedCompanies);
      if (i > -1) {
        $scope.includedCompanies.splice(i, 1);
      } else {
        $scope.includedCompanies.push(company);
      }
    }
  }

  var filters = [
    [{
      title: 'Philips',
      model: ''
    },{
      title: 'ResMed',
      model: ''
    },{
      title: 'Fisher & Paykel',
      model: ''
    }]
  ];

  var links = [{
    bind: 'Let Us Help You Pick A CPAP',
    sref: 'catalog.help'
  }];

})();

