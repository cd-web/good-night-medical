(function () {
  'use strict';

  var controllerId = 'catalogHelpPick';
  angular.module('app').controller(controllerId, ['$scope', 'FileUploader', catalogHelpPickCtrl]);

  function catalogHelpPickCtrl($scope, FileUploader) {
    $scope.links = links;

    $scope.currentPatient = true;
    $scope.diagnosedSleep = false;
    $scope.sleepStudy = false;
    $scope.prescription = false;
    $scope.currentMachine = 'Other';

    $scope.sleepStudyUploader = new FileUploader();
    $scope.prescriptionUploader = new FileUploader();

    $scope.sleepStudyUploader.onAfterAddingAll = function () {
      if ($scope.sleepStudyUploader.getNotUploadedItems().length > 1) $scope.sleepStudyUploader.removeFromQueue(0);
    };

    $scope.prescriptionUploader.onAfterAddingAll = function () {
      if ($scope.prescriptionUploader.getNotUploadedItems().length > 1) $scope.prescriptionUploader.removeFromQueue(0);
    };

    $scope.resetForm = function() {
      // TODO: Нужно сделать
    };

    $scope.submitHelpPick = function(){
      if ($scope.helpPick.$valid) {
        // Здесь должна быть функция отправки данных
        // Все ng-model данных в форме
        //
        // $scope.name  - Имя Фамилия (обязательное)
        // $scope.email - Почта       (обязательное)
        // $scope.phone - Номер       (обязательное)
        //
        // $scope.currentPatient - Пациент GNMK (от нее зависят следующие поля)
        // - YES { NOTHING }
        // - NO  {
        // -- $scope.address   - Адрес         (обязательное)
        // -- $scope.dateBirth - Дата рождения
        //
        // -- $scope.diagnosedSleep - Диагностировались ли вы (от нее зависят следующие поля)
        // --- NO  { NOTHING }
        // --- YES {
        // ---- $scope.currentMachine   - Текущая машина
        // ---- $scope.currentEquipment - Текушие оборудование (обязательное)
        // ---- $scope.currentMask      - Текущая маска        (обязательное)
        // ----
        // ---- $scope.sleepStudy - Копия исследования (от нее зависят следующие поля)
        // ----- YES { $scope.sleepStudyUploader - Изображение / Скан  }
        // ----- NO  { $scope.sleepTest          - Какая либа информация }
        // ----
        // ---- $scope.prescription - Рецепт (от нее зависят следующие поля)
        // ----- NO  { NOTHING }
        // ----- YES { $scope.prescriptionUploader - Документ, Скан рецепта }
        // --- }
        // - }
        //
        // $scope.message - Сообщение (любой текст от пользователя, пожелания или еще чего)
      }
    };
  }

  var links = [{
    bind: 'Let Us Help You Pick A CPAP',
    sref: 'catalog.help'
  },{
    bind: 'Catalog',
    sref: 'catalog.cpap'
  }];

})();

