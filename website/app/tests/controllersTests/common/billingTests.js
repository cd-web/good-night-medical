'use strict';

describe('billing Test', function () {
    beforeEach(module('app'));

    it('$scope.showErrorMsg - should return true if ((name.$touched && name.$invalid) || (name.$invalid && $scope.formInvalid))',
        inject(function ($rootScope, $controller) {
            var scope = $rootScope.$new();
            scope.formInvalid = false;
            var name = {
                $touched: true,
                $invalid: true
            };
            var ctrl = $controller("billing", { $scope: scope });
            expect(scope.showErrorMsg(name)).tobe(true);
            name.$touched = false;
            scope.formInvalid = true;
            expect(scope.showErrorMsg(name)).tobe(true);
        }));

    it('$scope.billErrorMsg - should return true if at least one call of $scope.showErrorMsg returns true',
        inject(function ($rootScope, $controller) {
            var scope = $rootScope.$new();
            scope.formInvalid = true;
            scope.billData = {
                cardNumber: 123456789012,
                cardMonth: 'January',
                cardYear: 2000,
                cardCvv: 123
            };
            scope.billData.cardNumber.$invalid = false;
            scope.billData.cardMonth.$invalid = false;
            scope.billData.cardYear.$invalid = false;
            scope.billData.cardCvv.$invalid = true;
            var ctrl = $controller("billing", { $scope: scope });
            expect(scope.billErrorMsg()).tobe(true);
        }));

    it('$scope.shipErrorMsg - should return true if at least one call of $scope.showErrorMsg returns true',
        inject(function ($rootScope, $controller) {
            var scope = $rootScope.$new();
            scope.formInvalid = true;
            scope.billData = {
                shipFirstName: 123456789012,
                shipLastName: January,
                shipAddress: 2000,
                shipCity: 123,
                shipState: 'USA',
                shipZip: 21343
            };
            scope.billData.shipFirstName.$invalid = false;
            scope.billData.shipLastName.$invalid = false;
            scope.billData.shipAddress.$invalid = false;
            scope.billData.shipCity.$invalid = false;
            scope.billData.shipState.$invalid = false;
            scope.billData.shipZip.$invalid = true;

            var ctrl = $controller("billing", { $scope: scope });
            expect(scope.shipErrorMsg()).tobe(true);
        }));

    it('$scope.billShipErrorMsg - should return true if at least one call of $scope.showErrorMsg returns true',
        inject(function ($rootScope, $controller) {
            var scope = $rootScope.$new();
            scope.formInvalid = true;
            scope.billData = {
                billFirstName: 123456789012,
                billLastName: January,
                billAddress: 2000,
                billCity: 123,
                billState: 'USA',
                billZip: 21343
            };
            scope.billData.billFirstName.$invalid = false;
            scope.billData.billLastName.$invalid = false;
            scope.billData.billAddress.$invalid = false;
            scope.billData.billCity.$invalid = false;
            scope.billData.billState.$invalid = false;
            scope.billData.billZip.$invalid = true;

            var ctrl = $controller("billing", { $scope: scope });
            expect(scope.billShipErrorMsg()).tobe(true);
        }));

    it('$scope.submitPurchase - should set true for $scope.formInvalid if $scope.billData.$invalid is true',
        inject(function ($rootScope, $controller) {
            var scope = $rootScope.$new();
            scope.formInvalid = false;
            scope.billData = {};
            scope.billData.$invalid = true;

            scope.submitPurchase();

            expect(scope.formInvalid).tobe(true);

        }));
});
