'use strict';

describe('main Test', function() {
    beforeEach(module('app'));

    var touched = true;
    var invalid = true;
    var form = true;

    it('$scope.returnClass - should return "error" if ((touched && invalid) || (invalid && form))', 
         inject(function ($rootScope, $controller, $timeout) {
             var scope = $rootScope.$new();
             var ctrl = $controller("main", { $scope: scope, $timeout });
             expect(scope.returnClass(touched, invalid, form)).toBe("error");
             touched = false;
             expect(scope.returnClass(touched, invalid, form)).toBe("error");
             touched = true;
             form = false;
             expect(scope.returnClass(touched, invalid, form)).toBe("error");
         }));

    it('$scope.showModal1Func - reverts value of corresponding property showModal1',
        inject(function($rootScope, $controller) {
            var scope = $rootScope.$new();
            scope.showModal1 = false;
            var ctrl = $controller("main", { $scope: scope });
            scope.showModal1Func();
            expect(scope.showModal1).tobe(true);

        }));

    it('$scope.showModal2Func - reverts value of corresponding property showModal2',
        inject(function ($rootScope, $controller) {
            var scope = $rootScope.$new();
            scope.showModal2 = false;
            var ctrl = $controller("main", { $scope: scope });
            scope.showModal2Func();
            expect(scope.showModal2).tobe(true);

        }));

    it('$scope.showModal3Func - reverts value of corresponding property showModal3',
        inject(function ($rootScope, $controller) {
            var scope = $rootScope.$new();
            scope.showModal3 = false;
            var ctrl = $controller("main", { $scope: scope });
            scope.showModal3Func();
            expect(scope.showModal3).tobe(true);

        }));

});