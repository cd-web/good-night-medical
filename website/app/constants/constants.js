(function (){
  'use strict';
  var app = angular.module('app');

  app.constant('globalConstants', {
    // apiUrl: "http://localhost/GenericBackend/api/"
    apiUrl: "http://192.169.142.119:8081/api/"
    // apiUrl: "http://localhost:54594/api/"
    // apiUrl: "http://goodnight-medical-demo.azurewebsites.net/"
  })
})();
