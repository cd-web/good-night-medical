var app = angular.module('app');

app.config(function($stateProvider, $urlRouterProvider) {

  $urlRouterProvider.otherwise("/");

  $stateProvider
    .state('home', {
      url: '/',
      templateUrl: 'app/templates/page1.html'
    })
    .state('rent', {
      url: '/rent',
      templateUrl: 'app/templates/page2.html'
    })
      .state('rent.catalog', {
        url: '/catalog',
        templateUrl: 'app/views/page2/catalog.html'
      })
      .state('rent.machine', {
        url: '/machine/:type',
        templateUrl: 'app/views/page2/machine.html'
      })
    .state('reSupply', {
      url: '/re-supply',
      templateUrl: 'app/templates/page3.html'
    })
      .state('reSupply.info', {
        url: '/info',
        templateUrl: 'app/views/page3/info.html'
      })
      .state('reSupply.form', {
        url: '/form',
        templateUrl: 'app/views/page3/form.html'
      })
    .state('catalog', {
      url: '/catalog',
      templateUrl: 'app/templates/page4.html'
    })
      .state('catalog.cpap', {
        url: '/cpap',
        templateUrl: 'app/views/page4/cpap.html'
      })
      .state('catalog.help', {
        url: '/help',
        templateUrl: 'app/views/page4/help.html'
      })
    .state('sleepTest', {
      url: '/sleep-test',
      templateUrl: 'app/templates/page5.html'
    })
      .state('sleepTest.info', {
        url: '/info',
        templateUrl: 'app/views/page5/info.html'
      })
    .state('information', {
      url: '/information',
      templateUrl: 'app/templates/information.html'
    })
      .state('information.aboutUs', {
        url: '/about-us',
        templateUrl: 'app/views/information/about-us.html'
      })
      .state('information.locations', {
        url: '/locations',
        templateUrl: 'app/views/information/locations.html'
      })
      .state('information.testimonials', {
        url: '/testimonials',
        templateUrl: 'app/views/information/testimonials.html'
      })
      .state('information.screeningTool', {
        url: '/screening-tool',
        templateUrl: 'app/views/information/screening-tool.html'
      })
    .state('common', {
      url: '/common',
      templateUrl: 'app/templates/common.html'
    })
      .state('common.billing', {
        url: '/billing',
        templateUrl: 'app/views/common/billing.html',
        params : { price: null, ships: '', type: null }
      })
      .state('common.thanks', {
        url: '/thanks',
        params: { shipment: null, billing: null },
        templateUrl: 'app/views/common/thanks.html'
      })
      .state('common.formThanks', {
        url: '/form-thanks',
        params: { fullName: '', email: '', phone: '', comments: '' },
        templateUrl: 'app/views/common/form-thanks.html'
      })
    .state('testing', {
      url: '/testing',
      templateUrl: 'app/templates/testing.html'
    })
      .state('testing.page1', {
        url: '/page1',
        templateUrl: 'app/views/testing/page1.html'
      })
});
