angular.module('dataServiceModule', []).factory('dataService', ['$http', 'globalConstants', dataService]);

function dataService($http, globalConstants) {
  var url = globalConstants.apiUrl;

  var service = {
    getMachines: getMachines,
    getMachinesByType: getMachinesByType,
    getCatalog: getCatalog,
    postContactUs: postContactUs,
    postRent: postRent,
    getRandomMachine: getRandomMachine,
    getMachinesCatalog: getMachinesCatalog,
    postPayment: postPayment,
    postReSupply: postReSupply,
    postScreening: postScreening,
    postHelpTestRequest: postHelpTestRequest
  };

  function getCatalog() {
    var subUrl = '/machines/catalog';
    return $http.get(url + subUrl);
  }

  function getMachines() {
    var destinationUrl = url + 'catalog';

    return $http.get(destinationUrl);
  }

  function getMachinesByType(type){
    var subUrl = '/machines?type=' + type;
    return $http.get(url + subUrl);
  }

  function postHelpTestRequest(model) {
    var destinationUrl = url + '/sleep-request';

    return $http.post(destinationUrl, model);
  }

  function postPayment(model) {
    var destinationUrl = url + '/payment';

    return $http.post(destinationUrl, model);
  }

  function postContactUs(model) {
    var destinationUrl = url + 'customer/contact-us/';

    return $http.post(destinationUrl, model);
  }

  function postRent(model) {
    var destinationUrl = url + 'customer/rent';

    return $http.post(destinationUrl, model)
  }

  function getRandomMachine() {
    var destinationUrl = url + 'machines/random/';

    return $http.get(destinationUrl);
  }

  function getMachinesCatalog() {
      var destinationUrl = url + 'machines';

      return $http.get(destinationUrl);
  }

  function postReSupply(model) {
      var destinationUrl = url + 'patient/resupply';
      return $http.post(destinationUrl, model);
  }

    function postScreening(model) {
        var destinationUrl = url + 'patient/screening';
        return $http.post(destinationUrl, model);
    }

    return service;
}