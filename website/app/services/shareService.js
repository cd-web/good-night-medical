angular.module('shareServiceModule', []).factory('shareService', [shareService]);

function shareService() {
  var machineId = '';
  var machineType = '';
  var isMachine = true;

  var addMachineId = function(id) {
    machineId = id;
  };

  var addMachineType = function (type) {
    machineType = type;
  };

  var getIsMachine = function () {
    return isMachine;
  };

  var setMachineType = function (type) {
    isMachine = type;
  };

  var getMachineId = function () {
    return machineId;
  };

  var getMachineType = function () {
    return machineType;
  };

  return {
    setIsMachine: setMachineType,
    isMachine: getIsMachine,
    addId : addMachineId,
    addType : addMachineType,
    getId: getMachineId,
    getType: getMachineType
  }
}

