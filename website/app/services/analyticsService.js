angular.module('analyticsServiceModule', []).factory('analyticsService', ['$http', 'globalConstants', analytics]);

function analytics($http, globalConstants) {
    var url = globalConstants.apiUrl;
    var service = {
        sendAnalytics: sendAnalytics
    };
    function sendAnalytics(model) {
       var destinationUrl = url + 'analytics';
       return $http.post(destinationUrl, model);
   }
    
    return service;
}
