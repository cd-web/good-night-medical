(function () {
    'use strict';

    var filterId = 'priceFilter';
    angular.module('app').filter(filterId, function() {

        return function( items, rangeInfo ) {
        var filtered = [];
            
        var min = parseInt(rangeInfo.minCost);
        var max = parseInt(rangeInfo.maxCost);
            
        angular.forEach(items, function(item) {
            if(item.pricePerMonth >= min && item.pricePerMonth <= max ) {
                filtered.push(item);
            }
        });
            
        return filtered;
    };
});
})();