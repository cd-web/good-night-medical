(function () {
  'use strict';

  var filterId = 'refurbishedFilter';
  angular.module('app').filter(filterId, function() {

    return function(items, isRefurbished) {
      var filtered = [];
      if(isRefurbished === false && filtered.length === 0){
        return items;
      }else {
        angular.forEach(items, function(item) {
          if (item.refurbished == isRefurbished) {
            filtered.push(item);
          }
        });

        return filtered;
      }
    };
  });
})();