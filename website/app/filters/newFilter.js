(function () {
  'use strict';

  var filterId = 'newFilter';
  angular.module('app').filter(filterId, function() {

    return function(items, isNew) {
      var filtered = [];
      if(isNew === false && filtered.length === 0){
        return items;
      }else {
        angular.forEach(items, function(item) {
          if (item.refurbished == !isNew) {
            filtered.push(item);
          }
        });

        return filtered;
      }
    };
  });
})();