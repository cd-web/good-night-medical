(function() {
  'use strict';

  var app = angular.module('app', [
    'ngAnimate',
    'ui.router',
    'ui.slider',
    'text.ellipsis',
    'angular-loading-bar',
    'dataServiceModule',
    'shareServiceModule',
    'angularFileUpload'
  ])
    .config(function($animateProvider) {
        // Анимация для определенных элементов (ngAnimate)
        $animateProvider.classNameFilter(/animate-on/);
    });

  // Handle routing errors and success events
  app.run(['$rootScope', function ($rootScope) {
    // При переходе на другую страницу пролистывает вверх
    $rootScope.$on('$stateChangeSuccess', function() {
       document.body.scrollTop = document.documentElement.scrollTop = 0;
    });
    // Include $route to kick start the router.
  }]);

})();
