(function () {
  'use strict';

  var controllerId = 'settings';

  angular.module('app').controller(controllerId, ['$scope', 'dataService', 'uiNotifications', settingController]);

  function settingController($scope, dataService, uiNotifications) {
    $scope.data = {};
    $scope.settingValue = false;
    $scope.changeState = changeState;
    
    populate();

    function populate() {
      dataService.getSettings().success(function (data){
          $scope.data = data;
      }).error(function(err) {
        console.error(err.stack);
      });
    }
    
    function changeState() {
      dataService.updateSetting($scope.data).success(function (){
        uiNotifications.success("Successfully updated setting.")
      }).error(function(err) {
        console.error(err.stack);
      });
    }
  }

})();