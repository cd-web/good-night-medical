(function () {
  'use strict';

  var controllerId = 'clientsDataBase';
  angular.module('app').controller(controllerId, ['$scope', clientsDataBaseCtrl]);

  function clientsDataBaseCtrl($scope) {
    $scope.clients = clients;
    $scope.modalInfo = false;

    $scope.showModalInfo = function() {
      $scope.modalInfo = !$scope.modalInfo;
    }
  }

  var clients = [{
    firstName: 'Eileen',
    lastName: 'Hamilton',
    gender: 'female',
    dateOfBirth: '7/12/1971',
    height: '154',
    weight: '68.1',
    email: 'jes.dix@mail.com',
    phone: '937-427-3380',
    socialSecurityNubmer: '290-48-4396',
    city: 'Dayton',
    state: 'Ohio',
    street: '4147 College Avenue',
    zipCode: '45434',
    data1: '11/11/2015',
    data2: '11/11/2016',
    docs: ['exmaple.pdf', 'exmaple.doc'],
    diagnosedObs: 'no',
    history: ['Sleep apnea1', 'example1'],
    moneyPaid: 895,
    modelPicked: [{
      model: 'CPAP1',
      data: '10/01/2017'
    },{
      model: 'CPAP2',
      data: '20/11/2016'
    },{
      model: 'CPAP3',
      data: '24/10/2015'
    }],
  },{
    firstName: 'Eileen',
    lastName: 'Hamilton',
    gender: 'female',
    height: '154',
    weight: '68.1',
    diagnosedObs: 'no',
    email: 'jes.dix@mail.com',
    phone: '937-427-3380',
    city: 'Dayton',
    state: 'Ohio',
    street: '4147 College Avenue',
    zipCode: '45434',
    dateOfBirth: '7/12/1971',
    socialSecurityNubmer: '290-48-4396',
    docs: ['exmaple.pdf', 'exmaple.doc'],
    data1: '11/11/2015',
    data2: '11/11/2016',
    moneyPaid: 895,
    history: ['Sleep apnea1', 'example1'],
    modelPicked: [{
      model: 'CPAP1',
      data: '10/01/2017'
    },{
      model: 'CPAP2',
      data: '20/11/2016'
    },{
      model: 'CPAP3',
      data: '24/10/2015'
    }],
  },{
    firstName: 'Eileen',
    lastName: 'Hamilton',
    gender: 'female',
    height: '154',
    weight: '68.1',
    diagnosedObs: 'no',
    email: 'jes.dix@mail.com',
    phone: '937-427-3380',
    city: 'Dayton',
    state: 'Ohio',
    street: '4147 College Avenue',
    zipCode: '45434',
    dateOfBirth: '7/12/1971',
    socialSecurityNubmer: '290-48-4396',
    docs: ['exmaple.pdf', 'exmaple.doc'],
    data1: '11/11/2015',
    data2: '11/11/2016',
    moneyPaid: 895,
    history: ['Sleep apnea1', 'example1'],
    modelPicked: [{
      model: 'CPAP1',
      data: '10/01/2017'
    },{
      model: 'CPAP2',
      data: '20/11/2016'
    },{
      model: 'CPAP3',
      data: '24/10/2015'
    }],
  }];

})();

