(function () {
    'use strict';

    var controllerId = 'analytics';
    angular.module('app').controller(controllerId, ['$scope', 'dataService', analyticsCtrl]);

    function analyticsCtrl($scope, dataService) {
        dataService.getAnalytics().success(function (data) {
            //$scope.content = data;
        });

        $scope.content = content;
        $scope.getAmount = function (index) {
            return $scope.content[index].analytics.length;
        }


        $scope.showModalInfo = function (index) {
            $scope.analyticsItem = $scope.content[index];
            $scope.modalInfo = !$scope.modalInfo;
        };

        $scope.showAllRequests = function () {
            $scope.requests = !$scope.requests;
        };
        $scope.getAnalyticsByRequest = function () {
            return $scope.requests ? $scope.analyticsItem.analytics
                : $scope.analyticsItem.analytics.slice(0, 10);
        }
    };

    //
    var content = [{

        landingPageUrl: 'http://programs.goodnightmedical.com/#/rent/catalog',
        request: 'q=sleep+apnea+machine+rent',
        requestText: 'sleep apnea machine rent',
        amount: 23,
        analytics: [{
            requestTime: new Date(),
            requestPageUrl: 'https://www.google.com.ua/search?q=sleep+apnea+machine+rent&ie=utf-8&oe=utf-8&client=firefox-b&gfe_rd=cr&ei=rPXHV5WNOaLP8geWkIXQBg',
            queryParameters: [
              {
                  key: 'q',
                  value: 'sleep+apnea+machine+rent'
              },
              {
                  key: 'ie',
                  value: 'utf-8'
              },
              {
                  key: 'client',
                  value: 'firefox-b'
              },
              {
                  key: 'gfe_rd',
                  value: 'cr'
              },
              {
                  key: 'ei',
                  value: 'rPXHV5WNOaLP8geWkIXQBg'
              }
            ]
        },
        {
            requestTime: new Date(),
            requestPageUrl: 'https://www.google.com.ua/search?q=sleep+apnea+machine+rent&ie=utf-8&oe=utf-8&client=firefox-b&gfe_rd=cr&ei=rPXHV5WNOaLP8geWkIXQBg',
            queryParameters: [
              {
                  key: 'q',
                  value: 'sleep+apnea+machine+rent'
              },
              {
                  key: 'ie',
                  value: 'utf-8'
              },
              {
                  key: 'client',
                  value: 'firefox-b'
              },
              {
                  key: 'gfe_rd',
                  value: 'cr'
              },
              {
                  key: 'ei',
                  value: 'rPXHV5WNOaLP8geWkIXQBg'
              }
            ]
        },
        {
            requestTime: new Date(),
            requestPageUrl: 'https://www.google.com.ua/search?q=sleep+apnea+machine+rent&ie=utf-8&oe=utf-8&client=firefox-b&gfe_rd=cr&ei=rPXHV5WNOaLP8geWkIXQBg',
            queryParameters: [
              {
                  key: 'q',
                  value: 'sleep+apnea+machine+rent'
              },
              {
                  key: 'ie',
                  value: 'utf-8'
              },
              {
                  key: 'client',
                  value: 'firefox-b'
              },
              {
                  key: 'gfe_rd',
                  value: 'cr'
              },
              {
                  key: 'ei',
                  value: 'rPXHV5WNOaLP8geWkIXQBg'
              }
            ]
        },
        {
            requestTime: new Date(),
            requestPageUrl: 'https://www.google.com.ua/search?q=sleep+apnea+machine+rent&ie=utf-8&oe=utf-8&client=firefox-b&gfe_rd=cr&ei=rPXHV5WNOaLP8geWkIXQBg',
            queryParameters: [
              {
                  key: 'q',
                  value: 'sleep+apnea+machine+rent'
              },
              {
                  key: 'ie',
                  value: 'utf-8'
              },
              {
                  key: 'client',
                  value: 'firefox-b'
              },
              {
                  key: 'gfe_rd',
                  value: 'cr'
              },
              {
                  key: 'ei',
                  value: 'rPXHV5WNOaLP8geWkIXQBg'
              }
            ]
        },
        {
            requestTime: new Date(),
            requestPageUrl: 'https://www.google.com.ua/search?q=sleep+apnea+machine+rent&ie=utf-8&oe=utf-8&client=firefox-b&gfe_rd=cr&ei=rPXHV5WNOaLP8geWkIXQBg',
            queryParameters: [
              {
                  key: 'q',
                  value: 'sleep+apnea+machine+rent'
              },
              {
                  key: 'ie',
                  value: 'utf-8'
              },
              {
                  key: 'client',
                  value: 'firefox-b'
              },
              {
                  key: 'gfe_rd',
                  value: 'cr'
              },
              {
                  key: 'ei',
                  value: 'rPXHV5WNOaLP8geWkIXQBg'
              }
            ]
        },
        {
            requestTime: new Date(),
            requestPageUrl: 'https://www.google.com.ua/search?q=sleep+apnea+machine+rent&ie=utf-8&oe=utf-8&client=firefox-b&gfe_rd=cr&ei=rPXHV5WNOaLP8geWkIXQBg',
            queryParameters: [
              {
                  key: 'q',
                  value: 'sleep+apnea+machine+rent'
              },
              {
                  key: 'ie',
                  value: 'utf-8'
              },
              {
                  key: 'client',
                  value: 'firefox-b'
              },
              {
                  key: 'gfe_rd',
                  value: 'cr'
              },
              {
                  key: 'ei',
                  value: 'rPXHV5WNOaLP8geWkIXQBg'
              }
            ]
        },
        {
            requestTime: new Date(),
            requestPageUrl: 'https://www.google.com.ua/search?q=sleep+apnea+machine+rent&ie=utf-8&oe=utf-8&client=firefox-b&gfe_rd=cr&ei=rPXHV5WNOaLP8geWkIXQBg',
            queryParameters: [
              {
                  key: 'q',
                  value: 'sleep+apnea+machine+rent'
              },
              {
                  key: 'ie',
                  value: 'utf-8'
              },
              {
                  key: 'client',
                  value: 'firefox-b'
              },
              {
                  key: 'gfe_rd',
                  value: 'cr'
              },
              {
                  key: 'ei',
                  value: 'rPXHV5WNOaLP8geWkIXQBg'
              }
            ]
        },
        {
            requestTime: new Date(),
            requestPageUrl: 'https://www.google.com.ua/search?q=sleep+apnea+machine+rent&ie=utf-8&oe=utf-8&client=firefox-b&gfe_rd=cr&ei=rPXHV5WNOaLP8geWkIXQBg',
            queryParameters: [
              {
                  key: 'q',
                  value: 'sleep+apnea+machine+rent'
              },
              {
                  key: 'ie',
                  value: 'utf-8'
              },
              {
                  key: 'client',
                  value: 'firefox-b'
              },
              {
                  key: 'gfe_rd',
                  value: 'cr'
              },
              {
                  key: 'ei',
                  value: 'rPXHV5WNOaLP8geWkIXQBg'
              }
            ]
        },
        {
            requestTime: new Date(),
            requestPageUrl: 'https://www.google.com.ua/search?q=sleep+apnea+machine+rent&ie=utf-8&oe=utf-8&client=firefox-b&gfe_rd=cr&ei=rPXHV5WNOaLP8geWkIXQBg',
            queryParameters: [
              {
                  key: 'q',
                  value: 'sleep+apnea+machine+rent'
              },
              {
                  key: 'ie',
                  value: 'utf-8'
              },
              {
                  key: 'client',
                  value: 'firefox-b'
              },
              {
                  key: 'gfe_rd',
                  value: 'cr'
              },
              {
                  key: 'ei',
                  value: 'rPXHV5WNOaLP8geWkIXQBg'
              }
            ]
        },
        {
            requestTime: new Date(),
            requestPageUrl: 'https://www.google.com.ua/search?q=sleep+apnea+machine+rent&ie=utf-8&oe=utf-8&client=firefox-b&gfe_rd=cr&ei=rPXHV5WNOaLP8geWkIXQBg',
            queryParameters: [
              {
                  key: 'q',
                  value: 'sleep+apnea+machine+rent'
              },
              {
                  key: 'ie',
                  value: 'utf-8'
              },
              {
                  key: 'client',
                  value: 'firefox-b'
              },
              {
                  key: 'gfe_rd',
                  value: 'cr'
              },
              {
                  key: 'ei',
                  value: 'rPXHV5WNOaLP8geWkIXQBg'
              }
            ]
        },
        {
            requestTime: new Date(),
            requestPageUrl: 'https://www.google.com.ua/search?q=sleep+apnea+machine+rent&ie=utf-8&oe=utf-8&client=firefox-b&gfe_rd=cr&ei=rPXHV5WNOaLP8geWkIXQBg',
            queryParameters: [
              {
                  key: 'q',
                  value: 'sleep+apnea+machine+rent'
              },
              {
                  key: 'ie',
                  value: 'utf-8'
              },
              {
                  key: 'client',
                  value: 'firefox-b'
              },
              {
                  key: 'gfe_rd',
                  value: 'cr'
              },
              {
                  key: 'ei',
                  value: 'rPXHV5WNOaLP8geWkIXQBg'
              }
            ]
        },
        {
            requestTime: new Date(),
            requestPageUrl: 'https://www.google.com.ua/search?q=sleep+apnea+machine+rent&ie=utf-8&oe=utf-8&client=firefox-b&gfe_rd=cr&ei=rPXHV5WNOaLP8geWkIXQBg',
            queryParameters: [
              {
                  key: 'q',
                  value: 'sleep+apnea+machine+rent'
              },
              {
                  key: 'ie',
                  value: 'utf-8'
              },
              {
                  key: 'client',
                  value: 'firefox-b'
              },
              {
                  key: 'gfe_rd',
                  value: 'cr'
              },
              {
                  key: 'ei',
                  value: 'rPXHV5WNOaLP8geWkIXQBg'
              }
            ]
        },
        {
            requestTime: new Date(),
            requestPageUrl: 'https://www.google.com.ua/search?q=sleep+apnea+machine+rent&ie=utf-8&oe=utf-8&client=firefox-b&gfe_rd=cr&ei=rPXHV5WNOaLP8geWkIXQBg',
            queryParameters: [
              {
                  key: 'q',
                  value: 'sleep+apnea+machine+rent'
              },
              {
                  key: 'ie',
                  value: 'utf-8'
              },
              {
                  key: 'client',
                  value: 'firefox-b'
              },
              {
                  key: 'gfe_rd',
                  value: 'cr'
              },
              {
                  key: 'ei',
                  value: 'rPXHV5WNOaLP8geWkIXQBg'
              }
            ]
        },
        {
            requestTime: new Date(),
            requestPageUrl: 'https://www.google.com.ua/search?q=sleep+apnea+machine+rent&ie=utf-8&oe=utf-8&client=firefox-b&gfe_rd=cr&ei=rPXHV5WNOaLP8geWkIXQBg',
            queryParameters: [
              {
                  key: 'q',
                  value: 'sleep+apnea+machine+rent'
              },
              {
                  key: 'ie',
                  value: 'utf-8'
              },
              {
                  key: 'client',
                  value: 'firefox-b'
              },
              {
                  key: 'gfe_rd',
                  value: 'cr'
              },
              {
                  key: 'ei',
                  value: 'rPXHV5WNOaLP8geWkIXQBg'
              }
            ]
        },
        {
            requestTime: new Date(),
            requestPageUrl: 'https://www.google.com.ua/search?q=sleep+apnea+machine+rent&ie=utf-8&oe=utf-8&client=firefox-b&gfe_rd=cr&ei=rPXHV5WNOaLP8geWkIXQBg',
            queryParameters: [
              {
                  key: 'q',
                  value: 'sleep+apnea+machine+rent'
              },
              {
                  key: 'ie',
                  value: 'utf-8'
              },
              {
                  key: 'client',
                  value: 'firefox-b'
              },
              {
                  key: 'gfe_rd',
                  value: 'cr'
              },
              {
                  key: 'ei',
                  value: 'rPXHV5WNOaLP8geWkIXQBg'
              }
            ]
        },
        {
            requestTime: new Date(),
            requestPageUrl: 'https://www.google.com.ua/search?q=sleep+apnea+machine+rent&ie=utf-8&oe=utf-8&client=firefox-b&gfe_rd=cr&ei=rPXHV5WNOaLP8geWkIXQBg',
            queryParameters: [
              {
                  key: 'q',
                  value: 'sleep+apnea+machine+rent'
              },
              {
                  key: 'ie',
                  value: 'utf-8'
              },
              {
                  key: 'client',
                  value: 'firefox-b'
              },
              {
                  key: 'gfe_rd',
                  value: 'cr'
              },
              {
                  key: 'ei',
                  value: 'rPXHV5WNOaLP8geWkIXQBg'
              }
            ]
        },
        {
            requestTime: new Date(),
            requestPageUrl: 'https://www.google.com.ua/search?q=sleep+apnea+machine+rent&ie=utf-8&oe=utf-8&client=firefox-b&gfe_rd=cr&ei=rPXHV5WNOaLP8geWkIXQBg',
            queryParameters: [
              {
                  key: 'q',
                  value: 'sleep+apnea+machine+rent'
              },
              {
                  key: 'ie',
                  value: 'utf-8'
              },
              {
                  key: 'client',
                  value: 'firefox-b'
              },
              {
                  key: 'gfe_rd',
                  value: 'cr'
              },
              {
                  key: 'ei',
                  value: 'rPXHV5WNOaLP8geWkIXQBg'
              }
            ]
        },
        {
            requestTime: new Date(),
            requestPageUrl: 'https://www.google.com.ua/search?q=sleep+apnea+machine+rent&ie=utf-8&oe=utf-8&client=firefox-b&gfe_rd=cr&ei=rPXHV5WNOaLP8geWkIXQBg',
            queryParameters: [
              {
                  key: 'q',
                  value: 'sleep+apnea+machine+rent'
              },
              {
                  key: 'ie',
                  value: 'utf-8'
              },
              {
                  key: 'client',
                  value: 'firefox-b'
              },
              {
                  key: 'gfe_rd',
                  value: 'cr'
              },
              {
                  key: 'ei',
                  value: 'rPXHV5WNOaLP8geWkIXQBg'
              }
            ]
        },
        {
            requestTime: new Date(),
            requestPageUrl: 'https://www.google.com.ua/search?q=sleep+apnea+machine+rent&ie=utf-8&oe=utf-8&client=firefox-b&gfe_rd=cr&ei=rPXHV5WNOaLP8geWkIXQBg',
            queryParameters: [
              {
                  key: 'q',
                  value: 'sleep+apnea+machine+rent'
              },
              {
                  key: 'ie',
                  value: 'utf-8'
              },
              {
                  key: 'client',
                  value: 'firefox-b'
              },
              {
                  key: 'gfe_rd',
                  value: 'cr'
              },
              {
                  key: 'ei',
                  value: 'rPXHV5WNOaLP8geWkIXQBg'
              }
            ]
        },
        {
            requestTime: new Date(),
            requestPageUrl: 'https://www.google.com.ua/search?q=sleep+apnea+machine+rent&ie=utf-8&oe=utf-8&client=firefox-b&gfe_rd=cr&ei=rPXHV5WNOaLP8geWkIXQBg',
            queryParameters: [
              {
                  key: 'q',
                  value: 'sleep+apnea+machine+rent'
              },
              {
                  key: 'ie',
                  value: 'utf-8'
              },
              {
                  key: 'client',
                  value: 'firefox-b'
              },
              {
                  key: 'gfe_rd',
                  value: 'cr'
              },
              {
                  key: 'ei',
                  value: 'rPXHV5WNOaLP8geWkIXQBg'
              }
            ]
        }]
    },
    {

        landingPageUrl: 'http://programs.goodnightmedical.com/#/rent/catalog',
        request: 'text=sleep%20apnea%20machine%20rent',
        requestText: 'sleep apnea machine rent',
        amount: 76,
        analytics: [{
            requestPageUrl: 'https://yandex.ua/search/?lr=147&msid=1472722851.25328.22901.29654&text=sleep%20apnea%20machine%20rent&p=0',
            requestTime: new Date(),
            queryParameters: [
              {
                  key: 'lr',
                  value: '147'
              },
              {
                  key: 'msid',
                  value: '1472722851.25328.22901.29654'
              },
              {
                  key: 'text',
                  value: 'sleep%20apnea%20machine%20rent'
              },
              {
                  key: 'p',
                  value: '0'
              }
            ]
        }]
    },
    {

        landingPageUrl: 'http://programs.goodnightmedical.com/#/rent/catalog',
        request: 'text=sleep%20apnea%20machine%20rent',
        requestText: 'sleep apnea machine rent',
        amount: 76,
        analytics: [{
            requestPageUrl: 'https://yandex.ua/search/?lr=147&msid=1472722851.25328.22901.29654&text=sleep%20apnea%20machine%20rent&p=0',
            requestTime: new Date(),
            queryParameters: [
              {
                  key: 'lr',
                  value: '147'
              },
              {
                  key: 'msid',
                  value: '1472722851.25328.22901.29654'
              },
              {
                  key: 'text',
                  value: 'sleep%20apnea%20machine%20rent'
              },
              {
                  key: 'p',
                  value: '0'
              }
            ]
        }]
    },
    {

        landingPageUrl: 'http://programs.goodnightmedical.com/#/rent/catalog',
        request: 'text=sleep%20apnea%20machine%20rent',
        requestText: 'sleep apnea machine rent',
        amount: 76,
        analytics: [{
            requestPageUrl: 'https://yandex.ua/search/?lr=147&msid=1472722851.25328.22901.29654&text=sleep%20apnea%20machine%20rent&p=0',
            requestTime: new Date(),
            queryParameters: [
              {
                  key: 'lr',
                  value: '147'
              },
              {
                  key: 'msid',
                  value: '1472722851.25328.22901.29654'
              },
              {
                  key: 'text',
                  value: 'sleep%20apnea%20machine%20rent'
              },
              {
                  key: 'p',
                  value: '0'
              }
            ]
        }]
    },
    {

        landingPageUrl: 'http://programs.goodnightmedical.com/#/rent/catalog',
        request: 'text=sleep%20apnea%20machine%20rent',
        requestText: 'sleep apnea machine rent',
        amount: 76,
        analytics: [{
            requestPageUrl: 'https://yandex.ua/search/?lr=147&msid=1472722851.25328.22901.29654&text=sleep%20apnea%20machine%20rent&p=0',
            requestTime: new Date(),
            queryParameters: [
              {
                  key: 'lr',
                  value: '147'
              },
              {
                  key: 'msid',
                  value: '1472722851.25328.22901.29654'
              },
              {
                  key: 'text',
                  value: 'sleep%20apnea%20machine%20rent'
              },
              {
                  key: 'p',
                  value: '0'
              }
            ]
        }]
    },
    {

        landingPageUrl: 'http://programs.goodnightmedical.com/#/rent/catalog',
        request: 'text=sleep%20apnea%20machine%20rent',
        requestText: 'sleep apnea machine rent',
        amount: 76,
        analytics: [{
            requestPageUrl: 'https://yandex.ua/search/?lr=147&msid=1472722851.25328.22901.29654&text=sleep%20apnea%20machine%20rent&p=0',
            requestTime: new Date(),
            queryParameters: [
              {
                  key: 'lr',
                  value: '147'
              },
              {
                  key: 'msid',
                  value: '1472722851.25328.22901.29654'
              },
              {
                  key: 'text',
                  value: 'sleep%20apnea%20machine%20rent'
              },
              {
                  key: 'p',
                  value: '0'
              }
            ]
        }]
    },
    {

        landingPageUrl: 'http://programs.goodnightmedical.com/#/rent/catalog',
        request: 'text=sleep%20apnea%20machine%20rent',
        requestText: 'sleep apnea machine rent',
        amount: 76,
        analytics: [{
            requestPageUrl: 'https://yandex.ua/search/?lr=147&msid=1472722851.25328.22901.29654&text=sleep%20apnea%20machine%20rent&p=0',
            requestTime: new Date(),
            queryParameters: [
              {
                  key: 'lr',
                  value: '147'
              },
              {
                  key: 'msid',
                  value: '1472722851.25328.22901.29654'
              },
              {
                  key: 'text',
                  value: 'sleep%20apnea%20machine%20rent'
              },
              {
                  key: 'p',
                  value: '0'
              }
            ]
        }]
    },
    {

        landingPageUrl: 'http://programs.goodnightmedical.com/#/rent/catalog',
        request: 'text=sleep%20apnea%20machine%20rent',
        requestText: 'sleep apnea machine rent',
        amount: 76,
        analytics: [{
            requestPageUrl: 'https://yandex.ua/search/?lr=147&msid=1472722851.25328.22901.29654&text=sleep%20apnea%20machine%20rent&p=0',
            requestTime: new Date(),
            queryParameters: [
              {
                  key: 'lr',
                  value: '147'
              },
              {
                  key: 'msid',
                  value: '1472722851.25328.22901.29654'
              },
              {
                  key: 'text',
                  value: 'sleep%20apnea%20machine%20rent'
              },
              {
                  key: 'p',
                  value: '0'
              }
            ]
        }]
    },
    {

        landingPageUrl: 'http://programs.goodnightmedical.com/#/rent/catalog',
        request: 'text=sleep%20apnea%20machine%20rent',
        requestText: 'sleep apnea machine rent',
        amount: 76,
        analytics: [{
            requestPageUrl: 'https://yandex.ua/search/?lr=147&msid=1472722851.25328.22901.29654&text=sleep%20apnea%20machine%20rent&p=0',
            requestTime: new Date(),
            queryParameters: [
              {
                  key: 'lr',
                  value: '147'
              },
              {
                  key: 'msid',
                  value: '1472722851.25328.22901.29654'
              },
              {
                  key: 'text',
                  value: 'sleep%20apnea%20machine%20rent'
              },
              {
                  key: 'p',
                  value: '0'
              }
            ]
        }]
    },
    {

        landingPageUrl: 'http://programs.goodnightmedical.com/#/rent/catalog',
        request: 'text=sleep%20apnea%20machine%20rent',
        requestText: 'sleep apnea machine rent',
        amount: 76,
        analytics: [{
            requestPageUrl: 'https://yandex.ua/search/?lr=147&msid=1472722851.25328.22901.29654&text=sleep%20apnea%20machine%20rent&p=0',
            requestTime: new Date(),
            queryParameters: [
              {
                  key: 'lr',
                  value: '147'
              },
              {
                  key: 'msid',
                  value: '1472722851.25328.22901.29654'
              },
              {
                  key: 'text',
                  value: 'sleep%20apnea%20machine%20rent'
              },
              {
                  key: 'p',
                  value: '0'
              }
            ]
        }]
    },
    {

        landingPageUrl: 'http://programs.goodnightmedical.com/#/rent/catalog',
        request: 'text=sleep%20apnea%20machine%20rent',
        requestText: 'sleep apnea machine rent',
        amount: 76,
        analytics: [{
            requestPageUrl: 'https://yandex.ua/search/?lr=147&msid=1472722851.25328.22901.29654&text=sleep%20apnea%20machine%20rent&p=0',
            requestTime: new Date(),
            queryParameters: [
              {
                  key: 'lr',
                  value: '147'
              },
              {
                  key: 'msid',
                  value: '1472722851.25328.22901.29654'
              },
              {
                  key: 'text',
                  value: 'sleep%20apnea%20machine%20rent'
              },
              {
                  key: 'p',
                  value: '0'
              }
            ]
        }]
    },
    {

        landingPageUrl: 'http://programs.goodnightmedical.com/#/rent/catalog',
        request: 'text=sleep%20apnea%20machine%20rent',
        requestText: 'sleep apnea machine rent',
        amount: 76,
        analytics: [{
            requestPageUrl: 'https://yandex.ua/search/?lr=147&msid=1472722851.25328.22901.29654&text=sleep%20apnea%20machine%20rent&p=0',
            requestTime: new Date(),
            queryParameters: [
              {
                  key: 'lr',
                  value: '147'
              },
              {
                  key: 'msid',
                  value: '1472722851.25328.22901.29654'
              },
              {
                  key: 'text',
                  value: 'sleep%20apnea%20machine%20rent'
              },
              {
                  key: 'p',
                  value: '0'
              }
            ]
        }]
    },
    {

        landingPageUrl: 'http://programs.goodnightmedical.com/#/rent/catalog',
        request: 'text=sleep%20apnea%20machine%20rent',
        requestText: 'sleep apnea machine rent',
        amount: 76,
        analytics: [{
            requestPageUrl: 'https://yandex.ua/search/?lr=147&msid=1472722851.25328.22901.29654&text=sleep%20apnea%20machine%20rent&p=0',
            requestTime: new Date(),
            queryParameters: [
              {
                  key: 'lr',
                  value: '147'
              },
              {
                  key: 'msid',
                  value: '1472722851.25328.22901.29654'
              },
              {
                  key: 'text',
                  value: 'sleep%20apnea%20machine%20rent'
              },
              {
                  key: 'p',
                  value: '0'
              }
            ]
        }]
    }, {

        landingPageUrl: 'http://programs.goodnightmedical.com/#/rent/catalog',
        request: 'text=sleep%20apnea%20machine%20rent',
        requestText: 'sleep apnea machine rent',
        amount: 76,
        analytics: [{
            requestPageUrl: 'https://yandex.ua/search/?lr=147&msid=1472722851.25328.22901.29654&text=sleep%20apnea%20machine%20rent&p=0',
            requestTime: new Date(),
            queryParameters: [
              {
                  key: 'lr',
                  value: '147'
              },
              {
                  key: 'msid',
                  value: '1472722851.25328.22901.29654'
              },
              {
                  key: 'text',
                  value: 'sleep%20apnea%20machine%20rent'
              },
              {
                  key: 'p',
                  value: '0'
              }
            ]
        }]
    },
    {

        landingPageUrl: 'http://programs.goodnightmedical.com/#/rent/catalog',
        request: 'text=sleep%20apnea%20machine%20rent',
        requestText: 'sleep apnea machine rent',
        amount: 76,
        analytics: [{
            requestPageUrl: 'https://yandex.ua/search/?lr=147&msid=1472722851.25328.22901.29654&text=sleep%20apnea%20machine%20rent&p=0',
            requestTime: new Date(),
            queryParameters: [
              {
                  key: 'lr',
                  value: '147'
              },
              {
                  key: 'msid',
                  value: '1472722851.25328.22901.29654'
              },
              {
                  key: 'text',
                  value: 'sleep%20apnea%20machine%20rent'
              },
              {
                  key: 'p',
                  value: '0'
              }
            ]
        }]
    }
    ];

})();

