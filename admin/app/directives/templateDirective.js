(function(){
  'use strict';

  angular
    .module('directive')
    .directive('uiTemplate', uiTemplate);
  
  function uiTemplate() {
    return {
      templateUrl: function(elem, attr) {
        return 'template-' + attr.type + '.html';
      }
    };
  }
})();
